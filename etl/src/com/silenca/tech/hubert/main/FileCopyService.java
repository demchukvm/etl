package com.silenca.tech.hubert.main;

import com.silenca.tech.hubert.utils.consts.PropertyNameConstants;
import com.silenca.tech.hubert.utils.consts.PropertyValueConstants;
import org.apache.camel.Exchange;
import org.apache.camel.util.FileUtil;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;

/**
 * @author Helena Bragina on 30.05.14.
 *         Service for copying file into success or failed directories by exchange property value.
 */
public class FileCopyService {

    public void copy(Exchange exchange) throws Exception {
        if (exchange.getContext().getProperties().containsKey(PropertyNameConstants.STATE_PROPERTY)) {
            String fileName = exchange.getIn().getHeader("CamelFileName", String.class);
            String filePath = exchange.getIn().getHeader("CamelFileAbsolutePath", String.class);
            File file = new File(filePath);

            String directoryName;

            if (exchange.getContext().getProperty(PropertyNameConstants.STATE_PROPERTY).equals(PropertyValueConstants.STATE_SUCCESS)) {
                directoryName = exchange.getContext().resolvePropertyPlaceholders("{{package.success}}");
            } else {
                directoryName = exchange.getContext().resolvePropertyPlaceholders("{{package.failed}}");
            }
            File parsedFile = new File(directoryName + File.separator + fileName);
            try {
                FileUtil.createNewFile(parsedFile);
            } catch (IOException e) {
                Logger.getLogger(getClass()).error("Path for saving Hubert files '"+ directoryName +"' is wrong or isn't specified. Please check conf/camel.properties " +
                        "package.success and package.failed values.");
                System.exit(0);
            }
            Logger.getLogger(getClass()).info(String.format("Copying file to directory %s..", directoryName));
            FileUtil.renameFile(file, parsedFile, true);
            exchange.getContext().getProperties().remove(PropertyNameConstants.STATE_PROPERTY);
            exchange.getContext().getProperties().remove(PropertyNameConstants.SUM_HEADER);
        }
    }
}
