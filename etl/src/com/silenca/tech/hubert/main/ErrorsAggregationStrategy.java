package com.silenca.tech.hubert.main;

import com.silenca.tech.hubert.utils.DateTimeUtils;
import com.silenca.tech.hubert.utils.consts.PropertyNameConstants;
import com.silenca.tech.hubert.utils.consts.PropertyValueConstants;
import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import javax.activation.DataHandler;

/**
 * @author Helena Bragina on 5/7/14 4:44 PM.
 */
public class ErrorsAggregationStrategy implements AggregationStrategy {

    /**
     * If errors text length will be more than this value, files with errors list attached to mail.
     */
    public static final int MAX_BODY_SIZE = 25600;

    private static final String EMPTY_BODY = "";

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        if (oldExchange == null) {
            if (newExchange.getProperties().containsKey(PropertyNameConstants.EXCHANGE_VALIDATION) &&
                    newExchange.getProperty(PropertyNameConstants.EXCHANGE_VALIDATION).equals(PropertyValueConstants.EXCHANGE_NOT_VALID)) {
                newExchange.getIn().setBody(replaceBrackets(newExchange.getIn().getBody(String.class)));
                return newExchange;
            }
            newExchange.getIn().setBody(EMPTY_BODY);
            return newExchange;
        }
        String oldBody = replaceBrackets(oldExchange.getIn().getBody(String.class));
        String newBody = replaceBrackets(newExchange.getIn().getBody(String.class));

        if (newExchange.getProperty(PropertyNameConstants.EXCHANGE_VALIDATION).equals(PropertyValueConstants.EXCHANGE_NOT_VALID)) {
            if (oldExchange.getProperty(PropertyNameConstants.EXCHANGE_VALIDATION).equals(PropertyValueConstants.EXCHANGE_NOT_VALID)) {
                if (newBody.contains(InvoiceTextIterator.LAST_INVOICE)) {
                    oldExchange.getIn().setBody(newBody + "\n" + oldBody);
                    if (oldExchange.getIn().getBody(String.class).length() > MAX_BODY_SIZE) {
                        byte[] file = oldExchange.getIn().getBody(byte[].class);
                        oldExchange.getIn().addAttachment(DateTimeUtils.getCurrentDateTimeString() + "." + oldExchange.getIn().getHeader("CamelFileName", String.class), new DataHandler(file, "plain/text"));
                        newBody = newBody + "\nErrors list in attachments.";
                        oldExchange.getIn().setBody(newBody);
                    }
                } else {
                    oldExchange.getIn().setBody(oldBody + "\n" + newBody);
                }
            } else if (oldExchange.getProperty(PropertyNameConstants.EXCHANGE_VALIDATION).equals(PropertyValueConstants.EXCHANGE_VALID)) {
                return newExchange;
            }
        } else {
            if (oldExchange.getProperty(PropertyNameConstants.EXCHANGE_VALIDATION).equals(PropertyValueConstants.EXCHANGE_VALID)) {
                oldExchange.getIn().setBody(EMPTY_BODY);
            }
        }
        return oldExchange;
    }

    private String replaceBrackets(String string) {
        string = string.replace("[", "");
        return string.replace("]", "");
    }

}
