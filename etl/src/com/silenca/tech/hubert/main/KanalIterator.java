package com.silenca.tech.hubert.main;

import java.util.Iterator;
import java.util.List;

/**
 * @author Helena Bragina on 5/6/14 9:54 PM.
 */
public class KanalIterator implements Iterator<String> {

    private static final byte FIRST_ELEMENT_INDEX = 0;

    private List<String> invoiceStrings;

    public KanalIterator(List<String> strings) {
        this.invoiceStrings = strings;
    }

    @Override
    public boolean hasNext() {
        return !invoiceStrings.isEmpty();
    }

    @Override
    public String next() {
        if (!invoiceStrings.isEmpty()) {
            String result = invoiceStrings.get(FIRST_ELEMENT_INDEX);
            remove();
            return result;
        }
        return null;
    }

    @Override
    public void remove() {
        if (!invoiceStrings.isEmpty()) {
            invoiceStrings.remove(FIRST_ELEMENT_INDEX);
        }
    }
}
