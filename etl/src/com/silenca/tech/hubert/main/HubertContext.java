package com.silenca.tech.hubert.main;

import com.silenca.tech.hubert.utils.consts.PropertyNameConstants;
import com.silenca.tech.hubert.utils.consts.PropertyValueConstants;
import org.apache.camel.*;
import org.apache.log4j.Logger;

/**
 * @author Helena Bragina
 *         Application entry point.
 */
public class HubertContext extends org.apache.camel.spring.Main {

    private static HubertContext context;

    public static void start(String[] args) {
        Logger.getLogger(HubertContext.class).info("-------------START-----------------");
        try {
            main(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void stop(String[] args) throws Exception {
        Logger.getLogger(HubertContext.class).info("---------STOP----------------");

        Endpoint endpoint = context.getCamelContexts().get(0).getEndpoint("smtps://smtp.gmail.com?username={{remote.e-mail}}&password={{remote.password}}&to=h.bragina@silencatech.com");
    //    Endpoint endpoint = context.getCamelContexts().get(0).getEndpoint(HubertRoutes.mailEndPoint);
        Exchange exchange = endpoint.createExchange();
        exchange.getContext().getProperties().put(PropertyNameConstants.STATE_PROPERTY, PropertyValueConstants.STATE_FAILED);
        exchange.setProperty(PropertyNameConstants.EXCHANGE_VALIDATION, PropertyValueConstants.EXCHANGE_NOT_VALID);
        exchange.getIn().setBody(InvoiceTextIterator.LAST_INVOICE, String.class);
        Logger.getLogger(HubertContext.class).info("Clearing table");
        DBEndpointService bean = (DBEndpointService) context.getApplicationContext().getBean("dbEndpointService");
        bean.save(exchange);

        System.exit(0);
    }


    public static void main(String[] args) throws Exception {
        context = new HubertContext();
        context.run(args);
    }
}
