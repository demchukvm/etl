package com.silenca.tech.hubert.exps;

import org.apache.log4j.Logger;

/**
 * @author Helena Bragina on 4/15/14 7:58 PM. Exception throws when file has no correct format.
 */
public class KanalIllegalFormatException extends Exception {

    public KanalIllegalFormatException(String message) {
        super(message);
        Logger.getLogger(getClass()).error(message);
    }

    public KanalIllegalFormatException(String message, Object... args) {
        super(String.format(message, args));
        Logger.getLogger(getClass()).error(String.format(message, args));
    }
}
