package com.silenca.tech.hubert.entities.kanals;

/**
 * @author Helena Bragina on 4/15/14 6:34 PM.
 */
public class Kanal1 implements Kanal {

    public static final String KANAL_NUMBER = "10";

    private String invoiceType;

    private String pageDefinition;

    private long invoiceNumber;

    private String clientsName;

    private String address1;

    private String address2;

    private String postNumber;

    private String country;

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getPageDefinition() {
        return pageDefinition;
    }

    public void setPageDefinition(String pageDefinition) {
        this.pageDefinition = pageDefinition;
    }

    public long getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(long invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getClientsName() {
        return clientsName;
    }

    public void setClientsName(String clientsName) {
        this.clientsName = clientsName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPostNumber() {
        return postNumber;
    }

    public void setPostNumber(String postNumber) {
        this.postNumber = postNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Kanal1{" +
                "invoiceType='" + invoiceType + '\'' +
                ", pageDefinition='" + pageDefinition + '\'' +
                ", invoiceNumber=" + invoiceNumber +
                ", clientsName='" + clientsName + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", postNumber='" + postNumber + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
