package com.silenca.tech.hubert.entities.kanals;

/**
 * @author Helena Bragina on 4/15/14 7:10 PM.
 */
public class Kanal9 implements Kanal {

    public static final String KANAL_NUMBER = "90";

    private String KIDNumber;

    private int croner;

    private int ore;

    private byte checkSum;

    public byte getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(byte checkSum) {
        this.checkSum = checkSum;
    }

    public int getOre() {
        return ore;
    }

    public void setOre(int ore) {
        this.ore = ore;
    }

    public int getCroner() {
        return croner;
    }

    public void setCroner(int croner) {
        this.croner = croner;
    }

    public String getKIDNumber() {
        return KIDNumber;
    }

    public void setKIDNumber(String KIDNumber) {
        this.KIDNumber = KIDNumber;
    }

    @Override
    public String toString() {
        return "Kanal9{" +
                "KIDNumber='" + KIDNumber + '\'' +
                ", croner=" + croner +
                ", ore='" + ore + '\'' +
                ", checkSum=" + checkSum +
                '}';
    }
}
