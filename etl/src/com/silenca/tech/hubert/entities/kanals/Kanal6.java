package com.silenca.tech.hubert.entities.kanals;

/**
 * @author Helena Bragina on 4/15/14 7:02 PM.
 */
public class Kanal6 implements Kanal {

    public static final String KANAL_NUMBER = "60";

    private long consumption;

    private long deductedFromAccount;

    private long amountWithGSTAndCurrency;

    private long amountsWithoutGSTAndCurrency;

    private long amountsMBAOnCurrency;

    public long getConsumption() {
        return consumption;
    }

    public void setConsumption(long consumption) {
        this.consumption = consumption;
    }

    public long getDeductedFromAccount() {
        return deductedFromAccount;
    }

    public void setDeductedFromAccount(long deductedFromAccount) {
        this.deductedFromAccount = deductedFromAccount;
    }

    public long getAmountWithGSTAndCurrency() {
        return amountWithGSTAndCurrency;
    }

    public void setAmountWithGSTAndCurrency(long amountWithGSTAndCurrency) {
        this.amountWithGSTAndCurrency = amountWithGSTAndCurrency;
    }

    public long getAmountsWithoutGSTAndCurrency() {
        return amountsWithoutGSTAndCurrency;
    }

    public void setAmountsWithoutGSTAndCurrency(long amountsWithoutGSTAndCurrency) {
        this.amountsWithoutGSTAndCurrency = amountsWithoutGSTAndCurrency;
    }

    public long getAmountsMBAOnCurrency() {
        return amountsMBAOnCurrency;
    }

    public void setAmountsMBAOnCurrency(long amountsMBAOnCurrency) {
        this.amountsMBAOnCurrency = amountsMBAOnCurrency;
    }

    @Override
    public String toString() {
        return "Kanal6{" +
                "consumption=" + consumption +
                ", deductedFromAccount=" + deductedFromAccount +
                ", amountWithGSTAndCurrency=" + amountWithGSTAndCurrency +
                ", amountsWithoutGSTAndCurrency=" + amountsWithoutGSTAndCurrency +
                ", amountsMBAOnCurrency=" + amountsMBAOnCurrency +
                '}';
    }
}
