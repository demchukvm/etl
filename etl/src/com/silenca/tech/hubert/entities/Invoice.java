package com.silenca.tech.hubert.entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

/**
 * @author Helena Bragina on 4/28/14 7:03 PM.
 */
@Entity(name = "Invoice")
public class Invoice {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "INVOICE_NUMBER", length = 15)
    private long invoiceNumber;

    @Column(name = "TYPE", length = 35)
    private String invoiceType;

    @Column(name = "DEFINITION", length = 8)
    private String pageDefinition;

    @Column(name = "SUBSCRIPTION_TYPE")
    private String subscriptionType;

    @Column(name = "SUBSCRIPTION_NUMBER", length = 15)
    private long subscriptionNumber;

    @Column(name = "DATE", length = 10)
    private Date invoiceDate;

    @Column(name = "DUE_DATE", length = 10)
    private Date dueDate;

    @Column(name = "PERIOD", length = 23)
    private String period;

    @Column(name = "KID", length = 25)
    private String KID;

    @Column(name = "SUM")
    private long sum;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(referencedColumnName = "NUMBER", nullable = false)
    private Customer customer;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(referencedColumnName = "INVOICE_NUMBER", nullable = false)
    private List<Passage> passage;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(referencedColumnName = "INVOICE_NUMBER", nullable = false)
    private List<Summary> summary;

    @Column(name = "VALID", length = 1)
    private int valid = 1;

    //----------- Temporary field, wouldn't be saved to DB
    @Transient
    private String extraPageDefinition;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(long invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getPageDefinition() {
        return pageDefinition;
    }

    public void setPageDefinition(String pageDefinition) {
        this.pageDefinition = pageDefinition;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public long getSubscriptionNumber() {
        return subscriptionNumber;
    }

    public void setSubscriptionNumber(long subscriptionNumber) {
        this.subscriptionNumber = subscriptionNumber;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getKID() {
        return KID;
    }

    public void setKID(String KID) {
        this.KID = KID;
    }

    public long getSum() {
        return sum;
    }

    public void setSum(long sum) {
        this.sum = sum;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Passage> getPassage() {
        return passage;
    }

    public void setPassage(List<Passage> passage) {
        this.passage = passage;
    }

    public List<Summary> getSummary() {
        return summary;
    }

    public void setSummary(List<Summary> summary) {
        this.summary = summary;
    }


    public void setExtraPageDefinition(String extraPageDefinition) {
        this.extraPageDefinition = extraPageDefinition;
    }

    public String getExtraPageDefinition() {
        return extraPageDefinition;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "id=" + id +
                ", invoiceNumber=" + invoiceNumber +
                ", invoiceType='" + invoiceType + '\'' +
                ", pageDefinition='" + pageDefinition + '\'' +
                ", subscriptionType='" + subscriptionType + '\'' +
                ", subscriptionNumber=" + subscriptionNumber +
                ", invoiceDate=" + invoiceDate +
                ", dueDate=" + dueDate +
                ", period='" + period + '\'' +
                ", KID='" + KID + '\'' +
                ", sum=" + sum +
                ", customer=" + customer +
                ", passage=" + passage +
                ", summary=" + summary +
                ", extraPageDefinition='" + extraPageDefinition + '\'' +
                '}';
    }
}
