package com.silenca.tech.hubert.parsers;

import com.silenca.tech.hubert.entities.kanals.Kanal6;
import com.silenca.tech.hubert.exps.KanalIllegalFormatException;
import com.silenca.tech.hubert.utils.KanalParserUtil;

import java.util.List;

/**
 * @author Helena Bragina on 5/1/14 12:58 PM.
 */
public class Kanal6Parser implements Parsable {

    public static final int AMOUNT_LINE_INDEX = 0;
    public static final int UNUSED_SYMBOLS_COUNT = 4;

    public Kanal6 parse(List<String> strings, int lineCounter, String invoiceType) throws KanalIllegalFormatException {
        String string = KanalParserUtil.removeNewLineChar(strings.get(AMOUNT_LINE_INDEX), lineCounter);
        String amountsWithoutGSTAndCurrency = string.substring(UNUSED_SYMBOLS_COUNT).trim();
        Kanal6 kanal = new Kanal6();
        if (KanalParserUtil.parseDouble(amountsWithoutGSTAndCurrency)) {
            kanal.setAmountsWithoutGSTAndCurrency(KanalParserUtil.getOresFromStringKrones(amountsWithoutGSTAndCurrency));
        }
        return kanal;
    }
}
