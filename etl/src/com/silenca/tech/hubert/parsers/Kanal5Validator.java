package com.silenca.tech.hubert.parsers;

import com.silenca.tech.hubert.utils.consts.EPLConstants;

import java.util.List;

/**
 * @author Helena Bragina on 4/18/14 6:45 PM.
 *         Contains line numbers of 5 section in different types of invoices.
 */
public class Kanal5Validator {

    public static final String ERROR_KANAL_MSG = "Kanal 5, line";

    private static final byte EPL50008_12_24_28_MAX_LENGTH = 18;
    private static final byte EPL50016_MAX_LENGTH = 24;
    private static final byte EPL500XX_MAX_LENGTH = 51;
    private static final byte EPL50036_MAX_LENGTH = 31;
    private static final byte EPL50000_02_18_MAX_LENGTH = 9;

    public static boolean checkExtraPageLineCount(List<String> strings, String invoiceDefinition) {
        switch (invoiceDefinition) {
            case EPLConstants.EPL50000:
            case EPLConstants.EPL50002:
            case EPLConstants.EPL50018: {
                return strings.size() <= EPL50000_02_18_MAX_LENGTH;
            }
            case EPLConstants.EPL50004:
            case EPLConstants.EPL50026:
            case EPLConstants.EPL50038: {
                return strings.size() <= EPL500XX_MAX_LENGTH;
            }
            case EPLConstants.EPL50008:
            case EPLConstants.EPL50012:
            case EPLConstants.EPL50024:
            case EPLConstants.EPL50028: {
                return strings.size() <= EPL50008_12_24_28_MAX_LENGTH;
            }
            case EPLConstants.EPL50006: {
                return strings.size() <= EPL50016_MAX_LENGTH;
            }
            case EPLConstants.EPL50036: {
                return strings.size() <= EPL50036_MAX_LENGTH;
            }
            default: {
                return strings.size() <= EPL500XX_MAX_LENGTH;
            }
        }
    }

}
