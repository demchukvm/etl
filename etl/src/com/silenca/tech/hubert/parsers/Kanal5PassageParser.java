package com.silenca.tech.hubert.parsers;

import com.silenca.tech.hubert.entities.Passage;
import com.silenca.tech.hubert.entities.kanals.Kanal5;
import com.silenca.tech.hubert.exps.KanalIllegalFormatException;
import com.silenca.tech.hubert.utils.DateTimeUtils;
import com.silenca.tech.hubert.utils.KanalParserUtil;
import org.apache.log4j.Logger;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Helena Bragina on 5/5/14 7:53 PM.
 */
public class Kanal5PassageParser {

    public static final String PASSAGE_TABLE_HEADER = "50L.nr\\s*Passeringstidspunkt\\s*Bomstasjon\\s*Brikke nr.\\s*Reg.nr\\s*Beløp";

    //Constants for invoice with type EPL5004
    private static final int BEGIN_INDEX = 0;
    private static final int STRING_NUMBER_END_INDEX = 5;
    private static final int DATE_STRING_END_INDEX = 24;
    private static final int TOLL_GATE_BEGIN_INDEX = 25;
    private static final int TOLL_GATE_END_INDEX = 75;
    private static final int CHIP_NUMBER_BEGIN_INDEX = 76;
    private static final int CHIP_NUMBER_END_INDEX = 96;
    private static final int REG_NUMBER_BEGIN_INDEX = 97;
    private static final int REG_NUMBER_END_INDEX = 110;
    private static final int QUANTITY_BEGIN_INDEX = 111;
    private static final int QUANTITY_END_INDEX = 118;

    //Pattern used for writing section 5 in document entities EPL50024, EPL50026, EPL50028.
    private static final Pattern SECTION_PATTERN = Pattern.compile("^50[ 0-9,A-Z,a-z]{26} [0-3][0-9].[0-1][0-9].[0-9]{4} ([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9] [ A-Z,a-z]{20} [ \\d+\\.\\d+]{9}$");
    private static final int START_PLAZA_FIELD_INDEX = 2;
    private static final int END_PLAZA_FIELD_INDEX = 26;
    private static final int START_DATE_INDEX = 28;
    private static final int END_TIME_INDEX = 48;
    private static final int START_QUANTITY_INDEX = 69;
    private static final int END_QUANTITY_INDEX = 78;

    //Constants for invoice with type EPL50008, EPL50016
    private static final int START_PLAZA_FIELD_INDEX_08 = 0;
    private static final int END_PLAZA_FIELD_INDEX_08 = 25;
    private static final int START_DATE_INDEX_08 = 27;
    private static final int END_TIME_INDEX_08 = 48;
    private static final int START_QUANTITY_INDEX_08 = 63;

    /**
     * Returns section 5 for document entities EPL50004 specified in specification.
     *
     * @param strings
     * @param lineCounter
     * @return
     * @throws com.silenca.tech.hubert.exps.KanalIllegalFormatException
     */
    public List<Passage> parse(List<String> strings, int lineCounter) throws KanalIllegalFormatException {
        KanalParserUtil.checkLineForVoid(strings, lineCounter);
        List<Passage> entities = new ArrayList<>();
        for (String string : strings) {
            lineCounter++;
            if (string.length() >= QUANTITY_END_INDEX) {
                string = KanalParserUtil.removeNewLineChar(string, lineCounter);
                Passage entity = new Passage();
                String lineNumber = string.substring(BEGIN_INDEX, STRING_NUMBER_END_INDEX).trim();
                if (KanalParserUtil.parseInt(lineNumber)) {
                    String dateString = string.substring(STRING_NUMBER_END_INDEX, DATE_STRING_END_INDEX).trim();
                    try {
                        entity.setTimestamp(DateTimeUtils.timestampFromString(dateString));
                    } catch (ParseException e) {
                        Logger.getLogger(Kanal5Validator.class).error("Cannot parse string " + dateString, e);
                        throw new KanalIllegalFormatException("%s: %s. Illegal timestamp format %s.", Kanal5Validator.ERROR_KANAL_MSG, lineCounter, dateString);
                    }
                    String tollGate = string.substring(TOLL_GATE_BEGIN_INDEX, TOLL_GATE_END_INDEX).trim();
                    if (!tollGate.isEmpty()) {
                        entity.setTollGate(tollGate);
                    } else {
                        throw new KanalIllegalFormatException("%s: %s. Tollgate isn't specified.", Kanal5Validator.ERROR_KANAL_MSG, lineCounter);
                    }
                    entity.setChipNumber(string.substring(CHIP_NUMBER_BEGIN_INDEX, CHIP_NUMBER_END_INDEX).trim());
                    String regNumber = string.substring(REG_NUMBER_BEGIN_INDEX, REG_NUMBER_END_INDEX).trim();
                    if (regNumber.matches(KanalParserUtil.REG_NUMBER_PATTERN)) {
                        entity.setRegNumber(regNumber);
                    } else {
                        throw new KanalIllegalFormatException("%s: %s. Registration number isn't specified.", Kanal5Validator.ERROR_KANAL_MSG, lineCounter);
                    }
                    String quantityString = string.substring(QUANTITY_BEGIN_INDEX, QUANTITY_END_INDEX).trim();
                    if (!quantityString.isEmpty()) {
                        entity.setAmount(KanalParserUtil.getOresFromStringKrones(quantityString));
                    } else {
                        throw new KanalIllegalFormatException("%s: %s. Quantity isn't specified.", Kanal5Validator.ERROR_KANAL_MSG, lineCounter);
                    }
                    entities.add(entity);
                }
            }
        }
        return entities;
    }

    /**
     * Returns section 5 for document entities EPL50024, EPL50026, EPL50028, EPL50124
     * specified in specification.
     *
     * @param strings
     * @param lineCounter
     * @return
     * @throws com.silenca.tech.hubert.exps.KanalIllegalFormatException
     */
    public List<Passage> parse24(List<String> strings, int lineCounter) throws KanalIllegalFormatException {
        KanalParserUtil.checkLineForVoid(strings, lineCounter);
        List<Passage> entities = new ArrayList<>();
        for (String string : strings) {
            Passage entity = new Passage();
            if ((string.startsWith(Kanal5.KANAL_NUMBER) || string.matches(KanalParserUtil.KANAL_DATA_REGEX)) && string.length() > 70) {
                entity.setTollGate(string.substring(START_PLAZA_FIELD_INDEX, END_PLAZA_FIELD_INDEX).trim());
                lineCounter++;
                String timestampString = string.substring(START_DATE_INDEX, END_TIME_INDEX).trim();
                lineCounter++;
                try {
                    entity.setTimestamp(DateTimeUtils.timestampFromString(timestampString));
                } catch (ParseException e) {
                    Logger.getLogger(Kanal5Validator.class).error("Cannot parse string " + timestampString, e);
                    throw new KanalIllegalFormatException("%s: %s. Illegal timestamp format %s.", Kanal5Validator.ERROR_KANAL_MSG, lineCounter, timestampString);
                }
                lineCounter++;
                String quantityString = string.substring(START_QUANTITY_INDEX, END_QUANTITY_INDEX).trim();
                if (KanalParserUtil.parseDouble(quantityString)) {
                    entity.setAmount(KanalParserUtil.getOresFromStringKrones(quantityString));
                } else {
                    throw new KanalIllegalFormatException("\"%s: %s. Kroner value %s isn't numeric.", Kanal5Validator.ERROR_KANAL_MSG, lineCounter, quantityString);
                }
                lineCounter++;
                entities.add(entity);
            } else {
                throw new KanalIllegalFormatException("\n%s: %s. Illegal kanal 5 format!", Kanal5Validator.ERROR_KANAL_MSG, lineCounter);
            }
        }
        return entities;
    }

    /**
     * Returns section 5 for document entities EPL50008, EPL50016
     * specified in specification.
     *
     * @param strings
     * @param lineCounter
     * @return
     * @throws com.silenca.tech.hubert.exps.KanalIllegalFormatException
     */
    public List<Passage> parse08(List<String> strings, int lineCounter) throws KanalIllegalFormatException {
        KanalParserUtil.checkLineForVoid(strings, lineCounter);
        List<Passage> entities = new ArrayList<>();
        for (String string : strings) {
            Passage entity = new Passage();
            if ((string.startsWith(Kanal5.KANAL_NUMBER) || string.matches(KanalParserUtil.KANAL_DATA_REGEX)) && string.length() > 70) {
                entity.setTollGate(string.substring(START_PLAZA_FIELD_INDEX_08, END_PLAZA_FIELD_INDEX_08).trim());
                lineCounter++;
                String timestampString = string.substring(START_DATE_INDEX_08, END_TIME_INDEX_08).trim();
                lineCounter++;
                try {
                    entity.setTimestamp(DateTimeUtils.timestampFromString(timestampString));
                } catch (ParseException e) {
                    Logger.getLogger(Kanal5Validator.class).error("Cannot parse string " + timestampString, e);
                    throw new KanalIllegalFormatException("%s: %s. Illegal timestamp format %s.", Kanal5Validator.ERROR_KANAL_MSG, lineCounter, timestampString);
                }
                lineCounter++;
                String quantityString = string.substring(START_QUANTITY_INDEX_08).trim();
                if (KanalParserUtil.parseDouble(quantityString)) {
                    entity.setAmount(KanalParserUtil.getOresFromStringKrones(quantityString));
                } else {
                    throw new KanalIllegalFormatException("\"%s: %s. Kroner value %s isn't numeric.", Kanal5Validator.ERROR_KANAL_MSG, lineCounter, quantityString);
                }
                lineCounter++;
                entities.add(entity);
            } else {
                throw new KanalIllegalFormatException("\n%s: %s. Illegal kanal 5 format!", Kanal5Validator.ERROR_KANAL_MSG, lineCounter);
            }
        }
        return entities;
    }
}
