package com.silenca.tech.hubert.utils;

import com.silenca.tech.hubert.utils.ClasspathResourceManager;

import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Helena Bragina on 5/13/14 4:35 PM.
 */
public class FileCreator {

    public static void main(String[] args) throws IOException, URISyntaxException {
        File file = ClasspathResourceManager.getResourceManager().getFile(".idea/test1.error.eBrev.txt");
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        int i = 0;
        List<String> strings = new ArrayList<>();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            if (strings.size() < 300000) {
                strings.add(line);
            } else {
                File file1 = new File("TestFile" + i + ".error.eBrev.txt");
                System.out.println("Created file " + file1.getAbsolutePath());
                FileWriter writer = new FileWriter(file1);
                String newLine = System.getProperty("line.separator");
                for (String s : strings) {
                    writer.write(s + newLine);
                }
                writer.close();
                strings = new ArrayList<>();
            }
            i++;
        }
        fileReader.close();
    }
}
