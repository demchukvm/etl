package com.silenca.tech.hubert.utils.consts;

/**
 * @author Helena Bragina on 5/13/14 1:24 PM.
 */
public class PropertyValueConstants {

    public static final String MAIL_MESSAGE = "Hubert. Wrong invoices format in ";

    public static final String WRONG_FILE_MESSAGE = "Hubert. Wrong file ";

    public static final String STATE_SUCCESS = "success";
    public static final String STATE_FAILED = "failed";

    public static final String EXCHANGE_VALID = "exchangeValid";
    public static final String EXCHANGE_NOT_VALID = "exchangeNotValid";

    public static final String AGGREGATOR_HEADER_VALUE = "Yes";

    private PropertyValueConstants() {
    }

}
