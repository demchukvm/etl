package com.silenca.tech.hubert.utils.consts;


/**
 * @author Helena Bragina on 4/18/14 12:46 PM.
 *         Types of invoices/bills ("Factura")
 *         specified in 2010-631-CSN_013-05 Spesifikasjon - Postens ebrev format.pdf.
 */
public class EPLConstants {

    public static final String EPL1FJELL = "EPL1FJELL";

    public static final String EPLKNO = "EPLKNO";

    public static final String EPL50000 = "EPL50000";

    public static final String EPL50002 = "EPL50002";

    public static final String EPL50006 = "EPL50006";

    public static final String EPL50008 = "EPL50008";

    public static final String EPL50012 = "EPL50012";

    public static final String EPL50018 = "EPL50018";

    public static final String EPL50024 = "EPL50024";

    public static final String EPL50028 = "EPL50028";

    public static final String EPL50036 = "EPL50036";

    public static final String EPL50100 = "EPL50100";

    public static final String EPL50102 = "EPL50102";

    public static final String EPL50124 = "EPL50124";

    // Extra page type
    public static final String EPL50004 = "EPL50004";

    public static final String EPL50016 = "EPL50016";

    public static final String EPL50026 = "EPL50026";

    public static final String EPL50038 = "EPL50038";


    private EPLConstants() {
    }
}
