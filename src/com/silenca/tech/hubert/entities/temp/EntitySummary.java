package com.silenca.tech.hubert.entities.temp;

/**
 * @author Helena Bragina on 4/29/14 5:56 PM.
 *         Temporary entity for creating total persistent Summary for saving.
 */
public class EntitySummary {

    private String chipNumber;

    private String regNumber;

    private String refComment;

    private String project;

    private long rate;

    private long passages;

    private long sum;

    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getRefComment() {
        return refComment;
    }

    public void setRefComment(String refComment) {
        this.refComment = refComment;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public long getRate() {
        return rate;
    }

    public void setRate(long rate) {
        this.rate = rate;
    }

    public long getPassages() {
        return passages;
    }

    public void setPassages(long passages) {
        this.passages = passages;
    }

    public long getSum() {
        return sum;
    }

    public void setSum(long sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "EntitySummary{" +
                ", chipNumber='" + chipNumber + '\'' +
                ", regNumber='" + regNumber + '\'' +
                ", refComment='" + refComment + '\'' +
                ", project='" + project + '\'' +
                ", rate=" + rate +
                ", passages=" + passages +
                ", sum=" + sum +
                '}';
    }
}
