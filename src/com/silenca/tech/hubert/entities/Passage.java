package com.silenca.tech.hubert.entities;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @author Helena Bragina on 4/28/14 7:05 PM.
 */
@Entity(name = "Passage")
public class Passage {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "TIMESTAMP", length = 20)
    private Timestamp timestamp;

    @Column(name = "TOLL_GATE", length = 35)
    private String tollGate;

    @Column(name = "CHIP_NUMBER", length = 20)
    private String chipNumber;

    @Column(name = "REGISTRATION_NUMBER", length = 10)
    private String regNumber;

    @Column(name = "AMOUNT")
    private long amount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getTollGate() {
        return tollGate;
    }

    public void setTollGate(String tollGate) {
        this.tollGate = tollGate;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Passage{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", tollGate='" + tollGate + '\'' +
                ", chipNumber='" + chipNumber + '\'' +
                ", regNumber='" + regNumber + '\'' +
                ", amount=" + amount +
                '}';
    }
}
