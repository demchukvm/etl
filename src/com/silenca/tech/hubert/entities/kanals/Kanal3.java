package com.silenca.tech.hubert.entities.kanals;

import java.sql.Date;

/**
 * @author Helena Bragina on 4/15/14 6:42 PM.
 */
public class Kanal3 implements Kanal {

    public static final String KANAL_NUMBER = "30";

    private String subscriptionType;

    private String clientNumber;

    private long subscriptionNumber;

    private long invoiceNumber;

    private String invoiceNumberWithIndex;

    private Date invoiceDate;

    private String period;

    private Date paymentDeadline;

    private String registrationNumber;

    private String orgClientNr;

    private String operatorPhoneNumber;

    private String reference;

    private long balanceBilling;

    private long facturaNrPurres;

    private String ipswichOperator;

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public long getSubscriptionNumber() {
        return subscriptionNumber;
    }

    public void setSubscriptionNumber(long subscriptionNumber) {
        this.subscriptionNumber = subscriptionNumber;
    }

    public long getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(long invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceNumberWithIndex() {
        return invoiceNumberWithIndex;
    }

    public void setInvoiceNumberWithIndex(String invoiceNumberWithIndex) {
        this.invoiceNumberWithIndex = invoiceNumberWithIndex;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Date getPaymentDeadline() {
        return paymentDeadline;
    }

    public void setPaymentDeadline(Date paymentDeadline) {
        this.paymentDeadline = paymentDeadline;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getOperatorPhoneNumber() {
        return operatorPhoneNumber;
    }

    public void setOperatorPhoneNumber(String operatorPhoneNumber) {
        this.operatorPhoneNumber = operatorPhoneNumber;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public long getBalanceBilling() {
        return balanceBilling;
    }

    public void setBalanceBilling(long balanceBilling) {
        this.balanceBilling = balanceBilling;
    }

    public long getFacturaNrPurres() {
        return facturaNrPurres;
    }

    public void setFacturaNrPurres(long facturaNrPurres) {
        this.facturaNrPurres = facturaNrPurres;
    }

    public String getOrgClientNr() {
        return orgClientNr;
    }

    public void setOrgClientNr(String orgClientNr) {
        this.orgClientNr = orgClientNr;
    }

    public String getIpswichOperator() {
        return ipswichOperator;
    }

    public void setIpswichOperator(String ipswichOperator) {
        this.ipswichOperator = ipswichOperator;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    @Override
    public String toString() {
        return "Kanal3{" +
                "subscriptionType='" + subscriptionType + '\'' +
                ", clientNumber=" + clientNumber +
                ", subscriptionNumber=" + subscriptionNumber +
                ", invoiceNumber=" + invoiceNumber +
                ", invoiceNumberWithIndex='" + invoiceNumberWithIndex + '\'' +
                ", invoiceDate=" + invoiceDate +
                ", period='" + period + '\'' +
                ", paymentDeadline=" + paymentDeadline +
                ", registrationNumber='" + registrationNumber + '\'' +
                ", orgClientNr='" + orgClientNr + '\'' +
                ", operatorPhoneNumber='" + operatorPhoneNumber + '\'' +
                ", reference='" + reference + '\'' +
                ", balanceBilling=" + balanceBilling +
                ", facturaNrPurres=" + facturaNrPurres +
                ", ipswichOperator='" + ipswichOperator + '\'' +
                '}';
    }
}
