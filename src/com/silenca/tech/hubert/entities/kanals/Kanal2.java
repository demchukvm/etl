package com.silenca.tech.hubert.entities.kanals;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Helena Bragina on 4/15/14 6:35 PM.
 */
@Entity(name = "section2")
public class Kanal2 implements Kanal {

    public static final String KANAL_NUMBER = "20";
    public static final byte ARGS_MAX_SIZE = 7;
    public static final byte EPL50036_ARGS_SIZE = 5;

    private Long id;
    private String name;

    private String address1;

    private String address2;

    private String zipCode;

    private String country;

    private String organizationNumber;

    private String identificationNumber;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getOrganizationNumber() {
        return organizationNumber;
    }

    public void setOrganizationNumber(String organizationNumber) {
        this.organizationNumber = organizationNumber;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    @Override
    public String toString() {
        return "Kanal2{" +
                "name='" + name + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", country='" + country + '\'' +
                ", organizationNumber='" + organizationNumber + '\'' +
                ", identificationNumber='" + identificationNumber + '\'' +
                '}';
    }
}
