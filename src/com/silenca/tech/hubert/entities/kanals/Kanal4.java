package com.silenca.tech.hubert.entities.kanals;

/**
 * @author Helena Bragina on 4/23/14 12:13 PM.
 */
public class Kanal4 implements Kanal {

    public static final String KANAL_NUMBER = "40";

    private String additionalText;

    public String getAdditionalText() {
        return additionalText;
    }

    public void setAdditionalText(String additionalText) {
        this.additionalText = additionalText;
    }

    @Override
    public String toString() {
        return "Kanal4{" +
                "additionalText='" + additionalText + '\'' +
                '}';
    }
}
