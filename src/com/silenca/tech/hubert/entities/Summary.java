package com.silenca.tech.hubert.entities;

import javax.persistence.*;

/**
 * @author Helena Bragina on 4/28/14 7:05 PM.
 */
@Entity(name = "Summary")
public class Summary {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "CHIP_NUMBER", length = 20)
    private String chipNumber;

    @Column(name = "REGISTRATION_NUMBER", length = 10)
    private String regNumber;

    @Column(name = "REFERENCE_COMMENT", length = 35)
    private String refComment;

    @Column(name = "PROJECT")
    private String project;

    @Column(name = "RATE")
    private long rate;

    @Column(name = "TOTAL_PASSAGES")
    private long totalPassages;

    @Column(name = "TOTAL_SUM")
    private long totalSum;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    public void setChipNumber(String chipNumber) {
        this.chipNumber = chipNumber;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getRefComment() {
        return refComment;
    }

    public void setRefComment(String refComment) {
        this.refComment = refComment;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public long getRate() {
        return rate;
    }

    public void setRate(long rate) {
        this.rate = rate;
    }

    public long getTotalPassages() {
        return totalPassages;
    }

    public void setTotalPassages(long totalPassages) {
        this.totalPassages = totalPassages;
    }

    public long getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(long totalSum) {
        this.totalSum = totalSum;
    }

    @Override
    public String toString() {
        return "Summary{" +
                "id=" + id +
                ", chipNumber='" + chipNumber + '\'' +
                ", regNumber='" + regNumber + '\'' +
                ", refComment='" + refComment + '\'' +
                ", project='" + project + '\'' +
                ", rate=" + rate +
                ", totalPassages=" + totalPassages +
                ", totalSum=" + totalSum +
                '}';
    }
}
