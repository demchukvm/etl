package com.silenca.tech.hubert.utils;


import com.silenca.tech.hubert.exps.KanalIllegalFormatException;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Helena Bragina on 4/18/14 2:43 PM.
 *         Utulity class for working with date and time, timestamp. Creating and checking.
 */
public class DateTimeUtils {

    private static Matcher matcher;

    public static final String TIME24HOURS_STRING = "([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]";

    public static final String DATE_STRING = "(0?[1-9]|[12][0-9]|3[01]).(0?[1-9]|1[012]).((19|20)\\d\\d)";

    public static final String DATE_PATTERN = "dd.MM.yyyy";

    public static final String TIMESTAMP_PATTERN = "dd.MM.yyyy HH:mm:ss";

    public static final String MAIL_TIMESTAMP_PATTERN = "yyyy.MM.dd-HH:mm";

    /**
     * Creating Date object from string.
     *
     * @param dateString - not validated string value of date.
     * @return sql.Date object if string parsed well.
     * @throws java.text.ParseException
     */
    public static Date dateFromString(String dateString) throws KanalIllegalFormatException, ParseException {
        if (validateDate(dateString)) {
            DateFormat formatter = new SimpleDateFormat(DATE_PATTERN);
            java.util.Date date = formatter.parse(dateString);
            return new Date(date.getTime());
        }
        throw new KanalIllegalFormatException("String %s cannot be parsed to java.sql.Date", dateString);
    }

    public static Time timeFromString(String timeString) throws KanalIllegalFormatException {
        if (validateTime(timeString)) {
            return Time.valueOf(timeString);
        }
        throw new KanalIllegalFormatException("String %s cannot be parsed to java.sql.Time", timeString);
    }

    /**
     * Validate time in 24 hours format with regular expression.
     *
     * @param time - time address for validation.
     * @return true if valid time format,
     */
    public static boolean validateTime(final String time) {
        matcher = Pattern.compile(TIME24HOURS_STRING).matcher(time);
        return matcher.matches();
    }

    /**
     * Date Format dd.mm.yyyy Regular Expression Pattern. Check data for correct.
     *
     * @param date - date string value.
     * @return true if valid date format.
     */
    public static boolean validateDate(final String date) {
        matcher = Pattern.compile(DATE_STRING).matcher(date);

        if (matcher.matches()) {
            matcher.reset();
            if (matcher.find()) {
                String day = matcher.group(1);
                String month = matcher.group(2);
                int year = Integer.parseInt(matcher.group(3));

                if (day.equals("31") && (month.equals("4") || month.equals("6") || month.equals("9") ||
                        month.equals("11") || month.equals("04") || month.equals("06") || month.equals("09"))) {
                    return false; // only 1,3,5,7,8,10,12 has 31 days
                } else if (month.equals("2") || month.equals("02")) {
                    //leap year
                    if (year % 4 == 0) {
                        return !(day.equals("30") || day.equals("31"));
                    } else {
                        return !(day.equals("29") || day.equals("30") || day.equals("31"));
                    }
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param timestampString
     * @return java.sql.Timestamp object converted from string.
     * @throws ParseException
     */
    public static Timestamp timestampFromString(String timestampString) throws ParseException {
        DateFormat formatter = new SimpleDateFormat(TIMESTAMP_PATTERN);
        java.util.Date date = formatter.parse(timestampString);
        return new java.sql.Timestamp(date.getTime());
    }

    public static String getCurrentDateTimeString() {
        DateFormat dateFormat = new SimpleDateFormat(MAIL_TIMESTAMP_PATTERN);
        java.util.Date date = new java.util.Date();
        return dateFormat.format(date);
    }
}

