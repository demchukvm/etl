package com.silenca.tech.hubert.utils;

import com.silenca.tech.hubert.exps.KanalIllegalFormatException;
import com.silenca.tech.hubert.utils.consts.EPLConstants;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Helena Bragina on 4/15/14 8:50 PM.
 *         Utility class with methods common to almost all parsers.
 */
public class KanalParserUtil {

    public static final String START_KANAL_PATTERN = "[1-9]0.*";
    public static final Pattern INVOICE_TYPE_PATTERN = Pattern.compile("^EPL50[0-9]{3}$");
    public static final String ERROR_MSG = "Illegal kanal format!";
    public static final String KANAL_DATA_REGEX = " 0.*";
    public static final String REG_NUMBER_PATTERN = "[A-Z,0-9]{4,7}";

    private static final byte KANAL_DATA_LENGTH = 2;

    private KanalParserUtil() {
    }

    public static boolean parseInt(String stringNumber) {
        try {
            int number = Integer.parseInt(stringNumber);
            return true;
        } catch (NumberFormatException e) {
            Logger.getLogger(KanalParserUtil.class).trace(String.format("String %s cannot be converted to integer number", stringNumber), e);
        }
        return false;
    }

    public static boolean parseLong(String stringNumber) {
        try {
            long number = Long.parseLong(stringNumber);
            return true;
        } catch (NumberFormatException e) {
            Logger.getLogger(KanalParserUtil.class).trace(String.format("String %s cannot be converted to long number", stringNumber), e);
        }
        return false;
    }


    public static boolean parseDouble(String stringNumber) {
        try {
            Double number = Double.parseDouble(stringNumber);
            return true;
        } catch (NumberFormatException e) {
            Logger.getLogger(KanalParserUtil.class).trace(String.format("String %s cannot be converted to number", stringNumber), e);
        }
        return false;
    }

    public static boolean parseByte(String stringNumber) {
        try {
            int number = Byte.parseByte(stringNumber);
            return true;
        } catch (NumberFormatException e) {
            Logger.getLogger(KanalParserUtil.class).trace(String.format("String %s cannot be converted to byte number", stringNumber), e);
        }
        return false;
    }

    /**
     * Removing start kanal symbols for parsing value.
     *
     * @param string
     * @param lineCounter
     * @return
     * @throws KanalIllegalFormatException
     */
    public static String removeNewLineChar(String string, int lineCounter) throws KanalIllegalFormatException {
        if (string.length() >= KANAL_DATA_LENGTH) {
            if (string.matches(KANAL_DATA_REGEX) || string.matches(START_KANAL_PATTERN)) {
                return string.substring(KANAL_DATA_LENGTH);
            }
        }
        throw new KanalIllegalFormatException("%s Error found in the %s string while removing new line characters", ERROR_MSG, lineCounter);
    }

    /**
     * Checking kanal strings for void.
     *
     * @param strings
     * @param lineCounter
     * @throws KanalIllegalFormatException
     */
    public static void checkLineForVoid(List<String> strings, int lineCounter) throws KanalIllegalFormatException {
        for (String string : strings) {
            if (string == null || string.isEmpty()) {
                throw new KanalIllegalFormatException("Kanal  , line %s: String in file is empty!", lineCounter);
            }
        }
    }

    /**
     * @param kronesString
     * @return money amount in ores.
     */
    public static long getOresFromStringKrones(String kronesString) {
        return (long) (Double.parseDouble(kronesString) * 100);
    }

    /**
     * Checking if specified page definition exists in specification.
     *
     * @param value - page definition EPL50[1-9]{3}.
     * @return true if current page will be parsed.
     */
    public static boolean checkInvoiceType(String value) {
        switch (value) {
            case EPLConstants.EPL50000:
            case EPLConstants.EPL50002:
            case EPLConstants.EPL50004:
            case EPLConstants.EPL50006:
            case EPLConstants.EPL50008:
            case EPLConstants.EPL50012:
            case EPLConstants.EPL50016:
            case EPLConstants.EPL50018:
            case EPLConstants.EPL50024:
            case EPLConstants.EPL50026:
            case EPLConstants.EPL50028:
            case EPLConstants.EPL50036:
            case EPLConstants.EPL50038:
            case EPLConstants.EPL50100:
            case EPLConstants.EPL50102:
            case EPLConstants.EPL50124: {
                return true;
            }
        }
        return false;
    }
}

