package com.silenca.tech.hubert.utils.consts;

/**
 * @author Helena Bragina on 4/29/14 9:29 PM.
 */
public class PropertyNameConstants {

    public static final String PROPERTIES = "properties";

    public static final String MAIL_SUBJECT_TAG = "subject";

    public static final String SUM_HEADER = "totalSum";

    public static final String STATE_PROPERTY = "state";

    public static final String EXCHANGE_VALIDATION = "exchangeValidation";

    public static final String AGGREGATOR_HEADER = "error";

    private PropertyNameConstants() {
    }
}
