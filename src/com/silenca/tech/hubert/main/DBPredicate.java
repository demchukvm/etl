package com.silenca.tech.hubert.main;

import com.silenca.tech.hubert.utils.consts.PropertyNameConstants;
import com.silenca.tech.hubert.utils.consts.PropertyValueConstants;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;

/**
 * @author Helena Bragina on 5/15/14 11:50 AM.
 *         Checking if invoice could be saved to DB.
 */
public class DBPredicate implements Predicate {
    @Override
    public boolean matches(Exchange exchange) {
        return exchange.getContext().getProperty(PropertyNameConstants.STATE_PROPERTY)
                .equals(PropertyValueConstants.STATE_SUCCESS) &&
                !exchange.getIn().getBody(String.class).contains(InvoiceTextIterator.LAST_INVOICE);
    }
}
