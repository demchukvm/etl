package com.silenca.tech.hubert.main;

import com.silenca.tech.hubert.utils.consts.PropertyNameConstants;
import org.apache.camel.*;
import org.apache.camel.builder.ExpressionBuilder;
import org.apache.camel.spring.SpringRouteBuilder;
import org.apache.log4j.Logger;

/**
 * Created by andrey on 4/23/14.
 */
public class HubertRoutes extends SpringRouteBuilder {

    public static final String mailEndPoint = "{{remote.mail-protocol}}://{{remote.username}}@{{remote.mail-server}}?password={{remote.password}}&to={{remote.mail-recipient}}";

    @Override
    public void configure() throws Exception {
        testSMTP();

        onCompletion()
                .beanRef("dbEndpointService", "save")
                .beanRef("fileCopyService", "copy");

        from("file:{{package.sources}}?noop=true&idempotent=true&idempotentKey=${file:name}-${file:modified}")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        testSMTP();
                    }
                })
                .split(ExpressionBuilder.beanExpression(new InvoiceIteratorFactory(), "createIterator"))
                .streaming()
                .process(new ValidatorProcessor())
                .choice()
                .when(new DBPredicate())
                .to("jpa://com.silenca.tech.hubert.entities.Invoice, " +
                        "jpa://com.silenca.tech.hubert.entities.Customer," +
                        "jpa://com.silenca.tech.hubert.entities.Passage, " +
                        "jpa://com.silenca.tech.hubert.entities.Summary?consumer.transacted=true")
                .otherwise()
                .aggregate(header(PropertyNameConstants.AGGREGATOR_HEADER), new ErrorsAggregationStrategy())
                .completionPredicate(new ReportCompletionPredicate())
              .to(mailEndPoint);
   //             .to("smtps://smtp.gmail.com?username={{remote.e-mail}}&password={{remote.password}}&to=h.bragina@silencatech.com");

    }


    private void testSMTP() throws Exception {
        Endpoint endpoint = getContext().getEndpoint(mailEndPoint);
 //       Endpoint endpoint = getContext().getEndpoint("smtps://smtp.gmail.com?username={{remote.e-mail}}&password={{remote.password}}&to=h.bragina@silencatech.com");
        Exchange exchange = endpoint.createExchange();
        Message in = exchange.getIn();
        in.setBody("Hubert connected successfully.");
        in.setHeader(PropertyNameConstants.MAIL_SUBJECT_TAG, "Hubert connection test");
        Producer producer = endpoint.createProducer();
        producer.start();
        producer.process(exchange);

        if (exchange.getException() != null) {
            Logger.getLogger(getClass()).error("Wrong e-mail properties specified in conf/camel.properties. Please, correct mail settings. Hubert stopped..");
            getContext().stop();
            System.exit(1);
        }

        producer.stop();
    }

}
