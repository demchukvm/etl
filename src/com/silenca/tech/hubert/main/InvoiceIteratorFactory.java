package com.silenca.tech.hubert.main;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

/**
 * @author Helena Bragina on 5/6/14 11:55 AM.
 */
public class InvoiceIteratorFactory {

    public Iterator<List<String>> createIterator(final InputStream inputStream) {
        return new InvoiceTextIterator(inputStream);
    }

}