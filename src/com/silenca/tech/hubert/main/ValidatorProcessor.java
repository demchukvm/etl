package com.silenca.tech.hubert.main;

import com.silenca.tech.hubert.utils.DateTimeUtils;
import com.silenca.tech.hubert.utils.KanalParserUtil;
import com.silenca.tech.hubert.utils.consts.EPLConstants;
import com.silenca.tech.hubert.entities.Customer;
import com.silenca.tech.hubert.entities.Invoice;
import com.silenca.tech.hubert.entities.Passage;
import com.silenca.tech.hubert.entities.Summary;
import com.silenca.tech.hubert.entities.kanals.*;
import com.silenca.tech.hubert.exps.InvoiceIllegalFormatException;
import com.silenca.tech.hubert.exps.KanalIllegalFormatException;
import com.silenca.tech.hubert.parsers.*;
import com.silenca.tech.hubert.utils.consts.PropertyNameConstants;
import com.silenca.tech.hubert.utils.consts.PropertyValueConstants;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Helena Bragina on 12.04.2014. Processors prototype.
 */
public class ValidatorProcessor implements Processor {

    private static final byte FIRST_KANAL_LINE_INDEX = 0;
    public static final int NUMBER_OF_LINE_INDEX = 1;

    public static final byte EXCHANGE_BODY_MIN_SIZE = 1;

    private int lineCounter;

    private Invoice currentInvoice;

    private String documentIdentifier;

    private List<String> errorStrings;

    /**
     * Reading data from object exchange, processing data for further parsing.
     *
     * @param exchange - contains data from text.
     */
    @Override
    public void process(Exchange exchange) throws InvoiceIllegalFormatException, IOException, KanalIllegalFormatException {
        errorStrings = new ArrayList<>();
        exchange.getIn().setHeader(PropertyNameConstants.AGGREGATOR_HEADER, PropertyValueConstants.AGGREGATOR_HEADER_VALUE);
        if (!exchange.getContext().getProperties().containsKey(PropertyNameConstants.STATE_PROPERTY)) {
            exchange.getContext().getProperties().put(PropertyNameConstants.STATE_PROPERTY, PropertyValueConstants.STATE_SUCCESS);
        }

        List<String> msg = exchange.getIn().getBody(List.class);
        String documentName = exchange.getIn().getHeader(Exchange.FILE_NAME, String.class);
        if (msg.size() > EXCHANGE_BODY_MIN_SIZE) {
            setStartInvoiceLineNumber(msg);
            Logger.getLogger(getClass()).info("Start reading document " + documentName);
            currentInvoice = new Invoice();
            KanalIterator iterator = new KanalIterator(msg);
            do {
                lineCounter++;
                try {
                    while (iterator.hasNext()) {
                        String string = iterator.next();
                        if (string.isEmpty()) {
                            throw new KanalIllegalFormatException("Kanal  , line %s: String isn't contain any information.", lineCounter);
                        }
                        appendDocument(string, iterator);
                        lineCounter++;
                        if (!errorStrings.isEmpty() &&
                                !exchange.getContext().getProperty(PropertyNameConstants.STATE_PROPERTY).equals(PropertyValueConstants.STATE_FAILED)) {
                            exchange.getContext().getProperties().put(PropertyNameConstants.STATE_PROPERTY, PropertyValueConstants.STATE_FAILED);
                        }
                    }
                    if (exchange.getProperty(PropertyNameConstants.EXCHANGE_VALIDATION) == null
                            || (exchange.getProperty(PropertyNameConstants.EXCHANGE_VALIDATION) != null
                            && !exchange.getProperty(PropertyNameConstants.EXCHANGE_VALIDATION).equals(PropertyValueConstants.EXCHANGE_NOT_VALID))) {
                        exchange.setProperty(PropertyNameConstants.EXCHANGE_VALIDATION, PropertyValueConstants.EXCHANGE_VALID);
                        exchange.getIn().setBody(currentInvoice);
                    } else {
                        Logger.getLogger(getClass()).info("Current invoice cannot be saved to DB, because current document has illegal invoices format.");
                    }
                } catch (IOException e) {
                    Logger.getLogger(getClass()).trace("Cannot read file. Reason is:", e);
                } catch (KanalIllegalFormatException e) {
                    Logger.getLogger(getClass()).trace("Wrong file format.", e);
                    Logger.getLogger(getClass()).error(e.getMessage());
                    setErrorInvoice(e.getMessage(), exchange, PropertyValueConstants.MAIL_MESSAGE);
                }
            } while (iterator.hasNext());
            if (!errorStrings.isEmpty()) {
                setErrorInvoice(null, exchange, PropertyValueConstants.MAIL_MESSAGE);
            }

            if (exchange.getProperty(PropertyNameConstants.EXCHANGE_VALIDATION).equals(PropertyValueConstants.EXCHANGE_NOT_VALID)) {
                if (!exchange.getContext().getProperties().containsKey(PropertyNameConstants.SUM_HEADER)) {
                    exchange.getContext().getProperties().put(PropertyNameConstants.SUM_HEADER, String.valueOf(1));
                } else {
                    exchange.getContext().getProperties().put(PropertyNameConstants.SUM_HEADER,
                            String.valueOf(Integer.parseInt(exchange.getContext().getProperty(PropertyNameConstants.SUM_HEADER)) + 1));
                }
            }
        } else if (msg.contains(InvoiceTextIterator.LAST_INVOICE)) {
            if (exchange.getContext().getProperty(PropertyNameConstants.STATE_PROPERTY).equals(PropertyValueConstants.STATE_FAILED)) {
                exchange.setProperty(PropertyNameConstants.EXCHANGE_VALIDATION, PropertyValueConstants.EXCHANGE_NOT_VALID);
                errorStrings.add(String.format("File %s with identifier %s. Date: %s", documentName, documentIdentifier, DateTimeUtils.getCurrentDateTimeString()));
                int totalInvoices = Integer.parseInt(exchange.getProperty("CamelSplitIndex").toString());
                int validErrors = Integer.parseInt(exchange.getContext().getProperty(PropertyNameConstants.SUM_HEADER));
                int validSuccess = totalInvoices - validErrors;
                errorStrings.add(String.format("\nTotal invoices: %s, validated with success: %s, validated with errors: %s", totalInvoices, validSuccess, validErrors));
                errorStrings.add("\n" + msg.get(FIRST_KANAL_LINE_INDEX));
                exchange.getIn().setBody(errorStrings.toString());
                errorStrings.clear();
            } else {
                exchange.setProperty(PropertyNameConstants.EXCHANGE_VALIDATION, PropertyValueConstants.EXCHANGE_VALID);
                exchange.getIn().setBody(msg.get(FIRST_KANAL_LINE_INDEX), String.class);
            }
        } else if (msg.contains(InvoiceTextIterator.WRONG_FILE)) {
            errorStrings.add(String.format("File %s with identifier %s. Date: %s", documentName, documentIdentifier, DateTimeUtils.getCurrentDateTimeString()));
            setErrorInvoice(msg.get(FIRST_KANAL_LINE_INDEX), exchange, PropertyValueConstants.WRONG_FILE_MESSAGE);
            errorStrings.clear();
        }
    }

    private void setErrorInvoice(String msg, Exchange exchange, String mailHeader) {
        if (!exchange.getContext().getProperty(PropertyNameConstants.STATE_PROPERTY).equals(PropertyValueConstants.STATE_FAILED)) {
            exchange.getContext().getProperties().put(PropertyNameConstants.STATE_PROPERTY, PropertyValueConstants.STATE_FAILED);
        }
        if (msg != null) {
            if (!errorStrings.isEmpty()) {
                msg = "\n" + msg;
            }
            errorStrings.add(msg);
        }
        exchange.setProperty(PropertyNameConstants.EXCHANGE_VALIDATION, PropertyValueConstants.EXCHANGE_NOT_VALID);
        exchange.getIn().setHeader(PropertyNameConstants.MAIL_SUBJECT_TAG, mailHeader + exchange.getIn().getHeader(Exchange.FILE_NAME, String.class));
        exchange.getIn().setBody(errorStrings.toString());
    }

    /**
     * Set first invoice line number offset int current document.
     *
     * @param strings - last string contains offset number.
     */
    private void setStartInvoiceLineNumber(List<String> strings) {
        String lineNumber = strings.get(NUMBER_OF_LINE_INDEX);
        strings.remove(strings.get(NUMBER_OF_LINE_INDEX));
        lineCounter = Integer.parseInt(lineNumber);
    }

    /**
     * @param line     - line that contains data about Kanal.
     * @param iterator - used for getting all data in current section.
     * @throws com.silenca.tech.hubert.exps.KanalIllegalFormatException
     * @throws java.io.IOException
     */
    private void appendDocument(String line, Iterator<String> iterator) throws KanalIllegalFormatException, IOException {
        if (line.startsWith(EPLConstants.EPL1FJELL)) {
            documentIdentifier = line;
        } else if (line.startsWith(EPLConstants.EPLKNO)) {
            if (currentInvoice.getCustomer() == null) {
                currentInvoice.setCustomer(new Customer());
            }
            Logger.getLogger(getClass()).debug(String.format("Processing invoice %s", line));
            currentInvoice.getCustomer().setZipCode(HeaderParser.parseAreaCode(line, lineCounter));
        } else {
            switch (line.substring(0, 2)) {
                case Kanal1.KANAL_NUMBER: {
                    setKanal1(iterator, line);
                    break;
                }
                case Kanal2.KANAL_NUMBER: {
                    setKanal2(iterator, line);
                    break;
                }
                case Kanal3.KANAL_NUMBER: {
                    setKanal3(iterator, line);
                    break;
                }
                case Kanal4.KANAL_NUMBER: {
                    break;
                }
                case Kanal5.KANAL_NUMBER: {
                    setKanal5(iterator, line);
                    break;
                }
                case Kanal6.KANAL_NUMBER: {
                    setKanal6(iterator);
                    break;
                }
                case Kanal9.KANAL_NUMBER: {
                    setKanal9(line);
                    break;
                }
                default: {
                    throw new KanalIllegalFormatException("Kanal  , line %s. String: '%s' isn't contain any data about section.", lineCounter, line);
                }
            }
        }
    }

    //--------- Methods for creating input data for parsers that return Kanal entities ------------

    private void setKanal1(Iterator<String> iterator, String line) throws IOException, KanalIllegalFormatException {
        List<String> sectionStrings = new ArrayList<>();
        sectionStrings.add(line);
        int numberOfFirstKanalLine = lineCounter;
        sectionStrings.addAll(readFromFile(iterator, 1));
        Kanal1Parser parser = new Kanal1Parser();
        Kanal1 kanal1 = parser.parse(sectionStrings, numberOfFirstKanalLine, null);
        if (kanal1.getPageDefinition().equals(EPLConstants.EPL50036)) {
            sectionStrings.clear();
            sectionStrings.addAll(readFromFile(iterator, 1));
            parser.setAdditionalData(kanal1, sectionStrings, lineCounter);
        } else if (kanal1.getPageDefinition().equals(EPLConstants.EPL50004)
                || kanal1.getPageDefinition().equals(EPLConstants.EPL50016)
                || kanal1.getPageDefinition().equals(EPLConstants.EPL50026)
                || kanal1.getPageDefinition().equals(EPLConstants.EPL50038)) {
            Logger.getLogger(getClass()).debug(String.format("Added extra page to document %s", currentInvoice.getInvoiceType()));
            currentInvoice.setExtraPageDefinition(kanal1.getPageDefinition());
        } else {
            currentInvoice.setPageDefinition(kanal1.getPageDefinition() != null ? kanal1.getPageDefinition() : "");
            currentInvoice.setInvoiceType(kanal1.getInvoiceType());
        }
    }

    private void setKanal2(Iterator<String> iterator, String line) throws IOException, KanalIllegalFormatException {
        checkDocType();
        int numberOfFirstKanalLine = lineCounter;
        List<String> sectionStrings = new ArrayList<>();
        sectionStrings.add(line);
        int numberOfAttributes = Kanal2.ARGS_MAX_SIZE - 1;
        if (currentInvoice.getPageDefinition().equals(EPLConstants.EPL50036)) {
            numberOfAttributes = Kanal2.EPL50036_ARGS_SIZE - 1;
        }
        sectionStrings.addAll(readFromFile(iterator, numberOfAttributes));
        Kanal2 kanal2 = new Kanal2Parser().parse(sectionStrings, numberOfFirstKanalLine, null);
        currentInvoice.getCustomer().setOrgNumber(kanal2.getOrganizationNumber());
        currentInvoice.getCustomer().setAddress1(kanal2.getAddress1());
        currentInvoice.getCustomer().setAddress2(kanal2.getAddress2());
        currentInvoice.getCustomer().setCountry(kanal2.getCountry());
        currentInvoice.getCustomer().setBirthDate(kanal2.getIdentificationNumber());
        currentInvoice.getCustomer().setName(kanal2.getName());
    }

    private void setKanal3(Iterator<String> iterator, String line) throws KanalIllegalFormatException, IOException {
        checkCustomer();
        int numberOfFirstKanalLine = lineCounter;
        List<String> sectionStrings = new ArrayList<>();
        sectionStrings.add(line);
        int numberOfAttributes = 5;
        String docBillType = currentInvoice.getPageDefinition();
        if (docBillType.equals(EPLConstants.EPL50012)) {
            numberOfAttributes = 6;
        }
        if (docBillType.equals(EPLConstants.EPL50002) || docBillType.equals(EPLConstants.EPL50102) || docBillType.equals(EPLConstants.EPL50018)) {
            numberOfAttributes = 7;
        } else if (docBillType.equals(EPLConstants.EPL50000) || docBillType.equals(EPLConstants.EPL50100)) {
            numberOfAttributes = 8;
        }
        sectionStrings.addAll(readFromFile(iterator, numberOfAttributes));
        Kanal3 kanal3 = new Kanal3Parser().parse(sectionStrings, numberOfFirstKanalLine, docBillType);
        if (kanal3 == null && (!docBillType.equals(EPLConstants.EPL50004) && !docBillType.equals(EPLConstants.EPL50016) &&
                !docBillType.equals(EPLConstants.EPL50026) && !docBillType.equals(EPLConstants.EPL50038))) {
            throw new KanalIllegalFormatException("Kanal 3, line: %s. Missed kanal 3 data. Line %s", lineCounter);
        } else {
            currentInvoice.setSubscriptionType(kanal3.getSubscriptionType());
            currentInvoice.setSubscriptionNumber(kanal3.getSubscriptionNumber());
            currentInvoice.setInvoiceDate(kanal3.getInvoiceDate());
            currentInvoice.setDueDate(kanal3.getPaymentDeadline());
            currentInvoice.setPeriod(kanal3.getPeriod());
            currentInvoice.setInvoiceNumber(kanal3.getInvoiceNumber());
            currentInvoice.getCustomer().setCustomerNumber(kanal3.getClientNumber());
        }
    }

    private void setKanal5(Iterator<String> iterator, String line) throws IOException, KanalIllegalFormatException {
        checkDocType();
        int numberOfFirstKanalLine = lineCounter;
        String docBillType = currentInvoice.getExtraPageDefinition() != null ? currentInvoice.getExtraPageDefinition() : currentInvoice.getPageDefinition();
        List<String> sectionStrings = new ArrayList<>();
        sectionStrings.add(line);
        String newLine = null;
        boolean read = true;
        while (read) {
            newLine = iterator.next();
            if (newLine != null) {
                lineCounter++;
                if (newLine.matches(KanalParserUtil.KANAL_DATA_REGEX)) {
                    sectionStrings.add(newLine);
                } else {
                    read = false;
                }
            } else {
                read = false;
            }
        }

        try {
            if (!Kanal5Validator.checkExtraPageLineCount(sectionStrings, docBillType)) {
                throw new KanalIllegalFormatException("Kanal 5, line: %s. Illegal count of lines.", lineCounter);
            }
            switch (docBillType) {
                case EPLConstants.EPL50008:
                case EPLConstants.EPL50016: {
                    List<Passage> passages = new Kanal5PassageParser().parse08(sectionStrings, numberOfFirstKanalLine);
                    if (currentInvoice.getPassage() == null) {
                        currentInvoice.setPassage(passages);
                    } else {
                        currentInvoice.getPassage().addAll(passages);
                    }
                    break;
                }
                case EPLConstants.EPL50024:
                case EPLConstants.EPL50026:
                case EPLConstants.EPL50028:
                case EPLConstants.EPL50124: {
                    List<Passage> passages = new Kanal5PassageParser().parse24(sectionStrings, numberOfFirstKanalLine);
                    if (currentInvoice.getPassage() == null) {
                        currentInvoice.setPassage(passages);
                    } else {
                        currentInvoice.getPassage().addAll(passages);
                    }
                    break;
                }
                default:
                    if (line.matches(Kanal5PassageParser.PASSAGE_TABLE_HEADER)) {
                        sectionStrings.remove(FIRST_KANAL_LINE_INDEX);
                        List<Passage> passages = new Kanal5PassageParser().parse(sectionStrings, numberOfFirstKanalLine);
                        if (currentInvoice.getPassage() == null) {
                            currentInvoice.setPassage(passages);
                        } else {
                            currentInvoice.getPassage().addAll(passages);
                        }
                    } else if (line.matches(Kanal5SummaryParser.SUMMARY_TABLE_HEADER)) {
                        sectionStrings.remove(FIRST_KANAL_LINE_INDEX);
                        List<Summary> summaries = new Kanal5SummaryParser().parse(sectionStrings, numberOfFirstKanalLine);
                        if (currentInvoice.getSummary() == null) {
                            currentInvoice.setSummary(summaries);
                        } else {
                            currentInvoice.getSummary().addAll(summaries);
                        }
                    }
                    break;
            }
        } catch (KanalIllegalFormatException e) {
            Logger.getLogger(getClass()).error(e.getMessage());
            String msg = e.getMessage();
            if (!errorStrings.isEmpty()) {
                msg = "\n" + msg;
            }
            errorStrings.add(msg);
        }
        if (newLine != null) {
            appendDocument(newLine, iterator);
        }
    }

    private void setKanal6(Iterator<String> iterator) throws IOException, KanalIllegalFormatException {
        if (currentInvoice.getPageDefinition().equals(EPLConstants.EPL50006)) {
            Kanal6 kanal6 = new Kanal6Parser().parse(readFromFile(iterator, 1), lineCounter, null);
            currentInvoice.setSum(currentInvoice.getSum() + kanal6.getAmountsWithoutGSTAndCurrency());
        }
    }

    private void setKanal9(String line) throws KanalIllegalFormatException {
        List<String> strings = new ArrayList<>();
        strings.add(line);
        Kanal9 kanal9 = new Kanal9Parser().parse(strings, lineCounter, null);
        currentInvoice.setKID(kanal9.getKIDNumber());
        currentInvoice.setSum(kanal9.getCroner() + kanal9.getOre());
    }

    /**
     * Read from BufferedReader specified count of strings.  Incremented lineCounter for sending reports about mistakes.
     *
     * @param iterator
     * @param lineCount
     * @return
     * @throws java.io.IOException
     */
    private List<String> readFromFile(Iterator<String> iterator, int lineCount) throws IOException, KanalIllegalFormatException {
        List<String> sectionStrings = new ArrayList<>();
        for (int i = 0; i < lineCount; i++) {
            if (iterator.hasNext()) {
                String nextLine = iterator.next();
                lineCounter++;
                if (!nextLine.matches(KanalParserUtil.START_KANAL_PATTERN)) {
                    sectionStrings.add(nextLine);
                } else {
                    throw new KanalIllegalFormatException("Kanal  , line: %s. Kanal has wrong number of strings.", lineCounter);
                }
            }
        }
        return sectionStrings;
    }

    /**
     * Checking if first section specified in file.
     *
     * @throws com.silenca.tech.hubert.exps.KanalIllegalFormatException
     */
    private void checkDocType() throws KanalIllegalFormatException {
        if (currentInvoice.getPageDefinition() == null) {
            currentInvoice.setPageDefinition("");
        }
    }

    /**
     * @return true if currentInvoice has Customer, false if extra page is checking.
     * @throws KanalIllegalFormatException if current invoice has not customer after parsing Kanal 2.
     */
    private boolean checkCustomer() throws KanalIllegalFormatException {
        switch (currentInvoice.getPageDefinition()) {
            case EPLConstants.EPL50004:
            case EPLConstants.EPL50016:
            case EPLConstants.EPL50026:
            case EPLConstants.EPL50038: {
                return false;
            }
            default: {
                if (currentInvoice.getCustomer() == null) {
                    throw new KanalIllegalFormatException("Kanal  , line: %s. Invoice customer wasn't specified", lineCounter);
                } else {
                    return true;
                }
            }
        }
    }
}
