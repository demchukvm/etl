package com.silenca.tech.hubert.main;

import com.silenca.tech.hubert.utils.consts.EPLConstants;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Helena Bragina on 5/6/14 11:57 AM.
 */
public class InvoiceTextIterator implements Iterator<List<String>> {

    public static final String LAST_INVOICE = "Errors found:";
    public static final String WRONG_FILE = "This file is not something that HUBERT expects to receive";

    private static final byte DOCUMENT_START_LINE = 0;
    private static final byte PREVIOUS_LINE_INDEX = 2;

    private BufferedReader bufferedReader;

    private List<String> nextInvoice;

    private String invoiceHeader;

    private int lineNumber = 0;

    private boolean lastInvoice = false;

    private boolean documentHeader;

    public InvoiceTextIterator(final InputStream inputStream) {
        bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        documentHeader = false;
    }

    @Override
    public boolean hasNext() {
        if (nextInvoice == null) {
            nextInvoice = readInvoice();
        }
        return nextInvoice != null;
    }

    @Override
    public List<String> next() {
        if (nextInvoice == null) {
            nextInvoice = readInvoice();
        }
        List<String> result = nextInvoice;
        nextInvoice = null;
        return result;
    }

    private List<String> readInvoice() {
        String line;
        List<String> strings = new ArrayList<>();
        if (invoiceHeader != null) {
            strings.add(invoiceHeader);
            strings.add(String.valueOf(lineNumber - 1));
            invoiceHeader = null;
        }
        try {
            int counter = 0;
            while ((line = bufferedReader.readLine()) != null) {
                lineNumber++;
                counter++;
                if (counter == 1) {
                    if (line.startsWith(EPLConstants.EPL1FJELL)) {
                        strings.add(line);
                        strings.add(String.valueOf(DOCUMENT_START_LINE));
                        documentHeader = true;
                    } else if (!documentHeader){
                        strings.add(WRONG_FILE);
                        closeBuffer();
                        lastInvoice = true;
                        return strings;
                    } else {
                        strings.add(line);
                    }
                } else {
                    if (line.startsWith(EPLConstants.EPLKNO) && strings.size() > 0 && !strings.get(counter - PREVIOUS_LINE_INDEX).startsWith(EPLConstants.EPL1FJELL)) {
                        invoiceHeader = line;
                        break;
                    }
                    strings.add(line);
                }
            }
        } catch (IOException e) {
            Logger.getLogger(getClass()).trace("Cannot read file. Reason is:", e);
        }
        if (strings.isEmpty()) {
            if (!lastInvoice) {
                lastInvoice = true;
                closeBuffer();
                if (lineNumber == 0) {
                    strings.add(WRONG_FILE);
                } else {
                    strings.add(LAST_INVOICE);
                }
                return strings;
            } else {
                return null;
            }
        }
        return strings;
    }

    private void closeBuffer() {
        try {
            bufferedReader.close();
            nextInvoice = null;
        } catch (IOException e) {
            Logger.getLogger(getClass()).trace("Cannot close buffered reader file. Reason is:", e);
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }


}

