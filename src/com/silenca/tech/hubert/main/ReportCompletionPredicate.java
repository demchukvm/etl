package com.silenca.tech.hubert.main;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;

/**
 * @author Helena Bragina on 5/22/14 9:12 PM.
 */
public class ReportCompletionPredicate implements Predicate {
    @Override
    public boolean matches(Exchange exchange) {
        boolean result1 = exchange.getIn().getBody(String.class).contains(InvoiceTextIterator.LAST_INVOICE); //||
        boolean result2 =  exchange.getIn().getBody(String.class).contains(InvoiceTextIterator.WRONG_FILE);
        return result1 || result2;
    }
}
