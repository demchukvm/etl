package com.silenca.tech.hubert.main;

import com.silenca.tech.hubert.utils.consts.PropertyNameConstants;
import com.silenca.tech.hubert.utils.consts.PropertyValueConstants;
import org.apache.camel.Exchange;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * @author Helena Bragina on 5/14/14 6:54 PM.
 *         Service used or rollback invoices entities if there was an error in document.
 */
public class DBEndpointService {

    public static final String UPDATE_QUERY = "UPDATE Invoice i SET i.valid = 0 WHERE i.valid = 1";
    public static final String DELETE_QUERY = "DELETE FROM Invoice i WHERE i.valid = 1";

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public void save(final Exchange exchange) throws Exception {
        if (exchange.getContext().getProperty(PropertyNameConstants.STATE_PROPERTY) != null) {
            if (entityManager == null)
                entityManager = entityManagerFactory.createEntityManager();

            TransactionTemplate transactionTemplate = exchange.getContext().getRegistry().lookupByNameAndType("transactionTemplate", TransactionTemplate.class);
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction(TransactionStatus status) {
                    entityManager.joinTransaction();
                    javax.persistence.Query query;
                    if (exchange.getContext().getProperty(PropertyNameConstants.STATE_PROPERTY).equals(PropertyValueConstants.STATE_SUCCESS)) {
                        query = entityManager.createQuery(UPDATE_QUERY);
                    } else {
                        query = entityManager.createQuery(DELETE_QUERY);
                    }
                    query.executeUpdate();
                    return null;
                }
            });
        }
    }
}
