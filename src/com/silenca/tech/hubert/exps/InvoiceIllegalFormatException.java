package com.silenca.tech.hubert.exps;

import org.apache.log4j.Logger;

/**
 * @author Helena Bragina on 5/1/14 12:19 PM.
 */
public class InvoiceIllegalFormatException extends Exception {

    public InvoiceIllegalFormatException(String message) {
        super(message);
        Logger.getLogger(getClass()).error(message);
    }

    public InvoiceIllegalFormatException(String message, Object... args) {
        super(String.format(message, args));
        Logger.getLogger(getClass()).error(String.format(message, args));
    }
}
