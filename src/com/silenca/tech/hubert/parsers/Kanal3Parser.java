package com.silenca.tech.hubert.parsers;

import com.silenca.tech.hubert.utils.KanalParserUtil;
import com.silenca.tech.hubert.utils.consts.EPLConstants;
import com.silenca.tech.hubert.entities.kanals.Kanal3;
import com.silenca.tech.hubert.exps.KanalIllegalFormatException;
import com.silenca.tech.hubert.utils.DateTimeUtils;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.text.ParseException;
import java.util.List;

/**
 * @author Helena Bragina on 4/18/14 11:50 AM.
 *         Section 3 may difference in dirrerent documents types.
 */
public class Kanal3Parser implements Parsable {

    private static final String CLIENTS_NUMBER_PATTERN = "^[0-9]{8,9}$";
    private static final String SUBSCRIPTION_NUMBER_PATTERN = "^[0-9]{10,11}$";
    private static final String BILL_NUMBER_PATTERN = "^[0-9]{1,15}$";
    private static final String PERIOD_PATTERN = "^[0-3][0-9].[0-1][0-9].[0-9]{4} - [0-3][0-9].[0-1][0-9].[0-9]{4}$";

    private static final String KANAL_ERROR = "Kanal 3, line";
    public static final int ZERO_LINE_INDEX = 0;
    public static final int FIRST_LINE_INDEX = 1;
    public static final int SECOND_LINE_INDEX = 2;
    public static final int THIRD_LINE_INDEX = 3;
    public static final int FOURTH_LINE_INDEX = 4;
    public static final int FIFTH_LINE_INDEX = 5;
    public static final int SIXTH_LINE_INDEX = 6;
    public static final int SEVENTH_LINE_INDEX = 7;
    public static final int EIGHTH_LINE_INDEX = 8;

    private String kanalTypeName;
    private int lineNumber;

    public Kanal3 parse(List<String> strings, int lineCounter, String kanalType) throws KanalIllegalFormatException {
        if (strings.size() >= 6) {
            kanalTypeName = kanalType;
            lineNumber = lineCounter;
            for (String string : strings) {
                if (string == null || string.isEmpty()) {
                    throw new KanalIllegalFormatException("%s: %s. String in file is empty!", KANAL_ERROR, lineCounter);
                }
            }
            return getSection(strings);
        }
        throw new KanalIllegalFormatException("%s: %s. Illegal kanal 3 format! Size of strings %s (< 6 as specified in specification).", KANAL_ERROR, lineCounter, strings.size());
    }

    private Kanal3 getSection(List<String> strings) throws KanalIllegalFormatException {
        switch (kanalTypeName) {
            case EPLConstants.EPL50000:
            case EPLConstants.EPL50100: {
                return parseEPL50000(strings);
            }
            case EPLConstants.EPL50002:
            case EPLConstants.EPL50018:
            case EPLConstants.EPL50102: {
                return parseEPL5002(strings);
            }
            case EPLConstants.EPL50006: {
                return parseEPL50006(strings);
            }
            case EPLConstants.EPL50008:
            case EPLConstants.EPL50024:
            case EPLConstants.EPL50028:
            case EPLConstants.EPL50124: {
                return parseEPL50008(strings);
            }
            case EPLConstants.EPL50012: {
                return parseEPL50012(strings);
            }
            case EPLConstants.EPL50036: {
                return parseEPL50036(strings);
            }
            default:{
                throw new KanalIllegalFormatException("%s: %s. Wrong invoice definition '%s.'", KANAL_ERROR, lineNumber, kanalTypeName);
            }
        }
    }

    private Kanal3 parseEPL50036(List<String> strings) throws KanalIllegalFormatException {
        Kanal3 kanal = new Kanal3();
        setOrgClientNr(kanal, strings.get(ZERO_LINE_INDEX));
        setInvoiceNumber(kanal, strings.get(FIRST_LINE_INDEX));
        setBillNumberWithIndex(kanal, strings.get(SECOND_LINE_INDEX));
        setBillDate(kanal, strings.get(THIRD_LINE_INDEX));
        setIpswichOperator(kanal, strings.get(FOURTH_LINE_INDEX));
        setOperatorPhone(kanal, strings.get(FIFTH_LINE_INDEX));
        return kanal;
    }

    private Kanal3 parseEPL50012(List<String> strings) throws KanalIllegalFormatException {
        Kanal3 kanal = new Kanal3();
        setInvoiceNumber(kanal, strings.get(FIRST_LINE_INDEX));
        setBillDate(kanal, strings.get(SECOND_LINE_INDEX));
        setPaymentDeadLine(kanal, strings.get(THIRD_LINE_INDEX));
        setRegistration(kanal, strings.get(FOURTH_LINE_INDEX));
        setClientsNumberRegistration(kanal, strings.get(FIFTH_LINE_INDEX));
        setReference(kanal, strings.get(SIXTH_LINE_INDEX));
        setFacturaNumber(kanal, strings.get(SEVENTH_LINE_INDEX));
        return kanal;
    }

    private Kanal3 parseEPL50006(List<String> strings) throws KanalIllegalFormatException {
        Kanal3 kanal = new Kanal3();
        setClientsNumber(kanal, strings.get(ZERO_LINE_INDEX));
        setSubscriptionNumber(kanal, strings.get(FIRST_LINE_INDEX));
        setInvoiceNumber(kanal, strings.get(SECOND_LINE_INDEX));
        setBillDate(kanal, strings.get(THIRD_LINE_INDEX));
        setPaymentDeadLine(kanal, strings.get(FOURTH_LINE_INDEX));
        setReference(kanal, strings.get(FIFTH_LINE_INDEX));
        return kanal;
    }

    /**
     * Parses kanal from such document entities as EPL50002, EPL50018.
     *
     * @param strings
     * @return
     */
    private Kanal3 parseEPL5002(List<String> strings) throws KanalIllegalFormatException {
        String subscriptionType = KanalParserUtil.removeNewLineChar(strings.get(ZERO_LINE_INDEX), lineNumber);
        if (subscriptionType.length() > 1) {
            Kanal3 kanal = new Kanal3();
            kanal.setSubscriptionType(subscriptionType);
            setClientsNumber(kanal, strings.get(FIRST_LINE_INDEX));
            setShortSubscriptionNumber(kanal, strings.get(SECOND_LINE_INDEX));
            setInvoiceNumber(kanal, strings.get(THIRD_LINE_INDEX));
            setBillDate(kanal, strings.get(FOURTH_LINE_INDEX));
            setPaymentDeadLine(kanal, strings.get(FIFTH_LINE_INDEX));
            String period = strings.get(SIXTH_LINE_INDEX);
            try {
                setPeriod(kanal, period);
            } catch (ParseException e) {
                Logger.getLogger(Kanal3Parser.class).error("Date specified in wrong format!", e);
                throw new KanalIllegalFormatException("%s: %s. Date specified in wrong format! %s instead of dd.mm.yyyy - dd.mm.yyyy.", KANAL_ERROR, lineNumber, period);
            }
            setReference(kanal, strings.get(SEVENTH_LINE_INDEX));
            return kanal;
        }
        throw new KanalIllegalFormatException("%s: %s. Missed subscription type.", KANAL_ERROR, lineNumber);
    }

    /**
     * Parses kanal from such document entities as EPL50008, EPL50024, EPL50028.
     *
     * @param strings
     * @return kanal 3 object with attributes specified in specification.
     */
    private Kanal3 parseEPL50008(List<String> strings) throws KanalIllegalFormatException {
        Kanal3 kanal = new Kanal3();
        setInvoiceNumber(kanal, strings.get(FIRST_LINE_INDEX));
        setBillDate(kanal, strings.get(SECOND_LINE_INDEX));
        setPaymentDeadLine(kanal, strings.get(THIRD_LINE_INDEX));
        setClientsNumberRegistration(kanal, strings.get(FOURTH_LINE_INDEX));
        setReference(kanal, strings.get(FIFTH_LINE_INDEX));
        return kanal;
    }

    /**
     * Parses kanal from bill type EPL50000
     *
     * @param strings
     * @return
     * @throws com.silenca.tech.hubert.exps.KanalIllegalFormatException
     */
    private Kanal3 parseEPL50000(List<String> strings) throws KanalIllegalFormatException {
        String subscriptionType = KanalParserUtil.removeNewLineChar(strings.get(ZERO_LINE_INDEX), lineNumber);
        if (subscriptionType.length() > 1) {
            Kanal3 kanal = new Kanal3();
            kanal.setSubscriptionType(subscriptionType);

            setClientsNumber(kanal, strings.get(FIRST_LINE_INDEX));
            setSubscriptionNumber(kanal, strings.get(SECOND_LINE_INDEX));
            setInvoiceNumber(kanal, strings.get(THIRD_LINE_INDEX));
            setBillDate(kanal, strings.get(FOURTH_LINE_INDEX));

            String period = strings.get(SIXTH_LINE_INDEX);
            try {
                setPeriod(kanal, period);
            } catch (ParseException e) {
                Logger.getLogger(Kanal3Parser.class).error("Date specified in wrong format!", e);
                throw new KanalIllegalFormatException("%s: %s. Period '%s' specified in wrong format!", KANAL_ERROR, lineNumber, period);
            }

            setReference(kanal, strings.get(SEVENTH_LINE_INDEX));
            setBillingBalance(kanal, strings.get(EIGHTH_LINE_INDEX));

            return kanal;
        }
        throw new KanalIllegalFormatException("%s: %s. Missed subscription type.", KANAL_ERROR, lineNumber);
    }

    //--------------------Setters for entity Section3-----------------------------------

    private void setClientsNumber(Kanal3 kanal, String clientsNumberLine) throws KanalIllegalFormatException {
        String clientsNumberString = KanalParserUtil.removeNewLineChar(clientsNumberLine, lineNumber);
        incLineCounter();
        if (clientsNumberString.matches(CLIENTS_NUMBER_PATTERN)) {
            kanal.setClientNumber(clientsNumberString);
        } else {
            throw new KanalIllegalFormatException("%s: %s. Clients number '%s' isn't numeric.", KANAL_ERROR, lineNumber, clientsNumberString);
        }
    }

    private void setClientsNumberRegistration(Kanal3 kanal, String clientsNumberLine) throws KanalIllegalFormatException {
        String clientsNumberString = KanalParserUtil.removeNewLineChar(clientsNumberLine, lineNumber);
        incLineCounter();
        if (!clientsNumberString.isEmpty()) {
            kanal.setClientNumber(clientsNumberString);
        } else {
            throw new KanalIllegalFormatException("%s: %s. Clients registration number isn' specified.", KANAL_ERROR, lineNumber);
        }
    }

    private void setSubscriptionNumber(Kanal3 kanal, String subscriptionNumberLine) throws KanalIllegalFormatException {
        String subscriptionNumberString = KanalParserUtil.removeNewLineChar(subscriptionNumberLine, lineNumber);
        incLineCounter();
        if (subscriptionNumberString.matches(SUBSCRIPTION_NUMBER_PATTERN)) {
            kanal.setSubscriptionNumber(Long.parseLong(subscriptionNumberString));
        } else {
            throw new KanalIllegalFormatException("%s: %s. Subscription '%s' number isn't numeric.", KANAL_ERROR, lineNumber, subscriptionNumberString);
        }
    }

    private void setShortSubscriptionNumber(Kanal3 kanal, String subscriptionNumberLine) throws KanalIllegalFormatException {
        String subscriptionNumberString = KanalParserUtil.removeNewLineChar(subscriptionNumberLine, lineNumber);
        incLineCounter();
        if (KanalParserUtil.parseInt(subscriptionNumberString)) {
            kanal.setSubscriptionNumber(Integer.parseInt(subscriptionNumberString));
        } else {
            throw new KanalIllegalFormatException("%s: %s. Subscription number '%s' isn't numeric.", KANAL_ERROR, lineNumber, subscriptionNumberString);
        }
    }

    private void setInvoiceNumber(Kanal3 kanal, String billNumberLine) throws KanalIllegalFormatException {
        String billNumberString = KanalParserUtil.removeNewLineChar(billNumberLine, lineNumber);
        incLineCounter();
        if (billNumberString.matches(BILL_NUMBER_PATTERN)) {
            kanal.setInvoiceNumber(Long.parseLong(billNumberString));
        } else {
            throw new KanalIllegalFormatException("%s: %s. Invoice number '%s' isn't numeric.", KANAL_ERROR, lineNumber, billNumberString);
        }
    }

    private void setBillDate(Kanal3 kanal, String billDateLine) throws KanalIllegalFormatException {
        billDateLine = KanalParserUtil.removeNewLineChar(billDateLine, lineNumber).trim();
        incLineCounter();
        try {
            Date billDate = DateTimeUtils.dateFromString(billDateLine);
            kanal.setInvoiceDate(billDate);
        } catch (ParseException | KanalIllegalFormatException e) {
            Logger.getLogger(Kanal3Parser.class).error("Date specified in wrong format!", e);
            throw new KanalIllegalFormatException("%s: %s. Invoice date '%s' specified in wrong format!", KANAL_ERROR, lineNumber, billDateLine);
        }
    }

    private void setPaymentDeadLine(Kanal3 kanal, String deadlineString) throws KanalIllegalFormatException {
        deadlineString = KanalParserUtil.removeNewLineChar(deadlineString, lineNumber).trim();
        incLineCounter();
        try {
            Date paymentDeadline = DateTimeUtils.dateFromString(deadlineString);
            kanal.setPaymentDeadline(paymentDeadline);
        } catch (ParseException | KanalIllegalFormatException e) {
            Logger.getLogger(Kanal3Parser.class).error("Date specified in wrong format!", e);
            throw new KanalIllegalFormatException("%s: %s. Invoice date '%s' specified in wrong format!", KANAL_ERROR, lineNumber, deadlineString);
        }
    }

    private void setPeriod(Kanal3 kanal, String periodLine) throws KanalIllegalFormatException, ParseException {
        periodLine = KanalParserUtil.removeNewLineChar(periodLine, lineNumber);
        incLineCounter();
        //TODO remove checking for empty string if period is required attribute
        if (!periodLine.isEmpty()) {
            if (periodLine.matches(PERIOD_PATTERN)) {
                String[] dates = periodLine.split("-");
                Date periodFrom = DateTimeUtils.dateFromString(dates[0].trim());
                Date periodTo = DateTimeUtils.dateFromString(dates[1].trim());
                kanal.setPeriod(periodLine);
            } else {
                throw new KanalIllegalFormatException("%s: %s. Wrong format of time period (%s).", KANAL_ERROR, lineNumber, periodLine);
            }
        }
    }

    private void setReference(Kanal3 kanal, String referenceLine) throws KanalIllegalFormatException {
        String clientReference = KanalParserUtil.removeNewLineChar(referenceLine, lineNumber);
        incLineCounter();
        if (!clientReference.isEmpty()) {
            kanal.setReference(clientReference);
        }
    }

    private void setBillingBalance(Kanal3 kanal, String balanceLine) throws KanalIllegalFormatException {
        String billingBalanceString = KanalParserUtil.removeNewLineChar(balanceLine, lineNumber);
        incLineCounter();
        if (billingBalanceString.length() > 1 && KanalParserUtil.parseDouble(billingBalanceString)) {
            long billingBalanceInDouble = KanalParserUtil.getOresFromStringKrones(billingBalanceString);
            kanal.setBalanceBilling(billingBalanceInDouble);
        } else {
            throw new KanalIllegalFormatException("%s: %s. Billing balance isn't specified!", KANAL_ERROR, lineNumber);
        }
    }

    private void setRegistration(Kanal3 kanal, String registrationLine) throws KanalIllegalFormatException {
        String registration = KanalParserUtil.removeNewLineChar(registrationLine, lineNumber);
        incLineCounter();
        if (!registration.isEmpty()) {
            kanal.setRegistrationNumber(registration);
        }
    }

    private void setFacturaNumber(Kanal3 kanal, String numberString) throws KanalIllegalFormatException {
        String numberFactura = KanalParserUtil.removeNewLineChar(numberString, lineNumber);
        incLineCounter();
        if (numberFactura.length() > 0) {
            if (KanalParserUtil.parseLong(numberFactura)) {
                kanal.setFacturaNrPurres(Long.parseLong(numberFactura));
            }
        } else {
            throw new KanalIllegalFormatException("%s: %s. Invoice number isn't specified!", KANAL_ERROR, lineNumber);
        }
    }

    private void setOrgClientNr(Kanal3 kanal, String numberString) throws KanalIllegalFormatException {
        String number = KanalParserUtil.removeNewLineChar(numberString, lineNumber);
        incLineCounter();
        if (!number.isEmpty()) {
            kanal.setClientNumber(number);
        }
    }

    private void setBillNumberWithIndex(Kanal3 kanal, String indexString) throws KanalIllegalFormatException {
        String index = KanalParserUtil.removeNewLineChar(indexString, lineNumber);
        incLineCounter();
        if (!index.isEmpty() && index.contains("-")) {
            kanal.setInvoiceNumberWithIndex(index);
        } else {
            throw new KanalIllegalFormatException("%s: %s. Index number of bill number isn't specified!", KANAL_ERROR, lineNumber);
        }
    }

    private void setIpswichOperator(Kanal3 kanal, String ipswichOperatorString) throws KanalIllegalFormatException {
        String ipswichOperator = KanalParserUtil.removeNewLineChar(ipswichOperatorString, lineNumber);
        incLineCounter();
        if (!ipswichOperator.isEmpty()) {
            kanal.setIpswichOperator(ipswichOperator);
        }
    }

    private void setOperatorPhone(Kanal3 kanal, String operatorPhoneString) throws KanalIllegalFormatException {
        String operatorPhone = KanalParserUtil.removeNewLineChar(operatorPhoneString, lineNumber);
        incLineCounter();
        if (!operatorPhone.isEmpty()) {
            if (operatorPhone.startsWith("+")) {
                kanal.setIpswichOperator(operatorPhone);
            } else {
                throw new KanalIllegalFormatException("%s: %s. Wrong format of telephone number '%s'.", KANAL_ERROR, lineNumber, operatorPhone);
            }
        }
    }

    private void incLineCounter() {
        lineNumber++;
    }
}