package com.silenca.tech.hubert.parsers;

import com.silenca.tech.hubert.entities.temp.EntitySummary;
import com.silenca.tech.hubert.entities.Summary;
import com.silenca.tech.hubert.exps.KanalIllegalFormatException;
import com.silenca.tech.hubert.utils.KanalParserUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Helena Bragina on 5/5/14 7:47 PM.
 */
public class Kanal5SummaryParser {

    public static final String SUMMARY_TABLE_HEADER = "50L.nr.\\s*Brikke nr.\\s*Reg.nr.\\s*Ref.\\s*Prosjekt\\s*Kurs\\s*Passeringer\\s*Til betaling.*";

    private static final byte BEGIN_INDEX = 1;
    private static final byte LINE_NUMBER_END_INDEX = 5;
    private static final byte CHIP_NUMBER_BEGIN_INDEX = 6;
    private static final byte CHIP_NUMBER_END_INDEX = 23;
    private static final byte REG_NUMBER_BEGIN_INDEX = 24;
    private static final byte REG_NUMBER_END_INDEX = 35;
    private static final byte REF_COMMENT_BEGIN_INDEX = 35;
    private static final byte REF_COMMENT_END_INDEX = 55;
    private static final byte PROJECT_BEGIN_INDEX = 56;
    private static final byte PROJECT_END_INDEX = 66;
    private static final byte COURSE_BEGIN_INDEX = 67;
    private static final byte COURSE_END_INDEX = 72;
    private static final byte PASSAGES_BEGIN_INDEX = 73;
    private static final byte PASSAGES_END_INDEX = 91;
    private static final byte PAYMENT_BEGIN_INDEX = 92;

    /**
     * @param sectionStrings
     * @param lineCounter
     * @return list of summaries for invoices with types: EPL50002.
     * @throws com.silenca.tech.hubert.exps.KanalIllegalFormatException
     */
    public List<Summary> parse(List<String> sectionStrings, int lineCounter) throws KanalIllegalFormatException {
        KanalParserUtil.checkLineForVoid(sectionStrings, lineCounter);
        List<EntitySummary> entities = new ArrayList<>();
        for (String string : sectionStrings) {
            lineCounter++;
            string = KanalParserUtil.removeNewLineChar(string, lineCounter);
            if (string.length() >= PAYMENT_BEGIN_INDEX) {
                EntitySummary entity = new EntitySummary();
                String number = string.substring(BEGIN_INDEX, LINE_NUMBER_END_INDEX).trim();
                String chipNumber = string.substring(CHIP_NUMBER_BEGIN_INDEX, CHIP_NUMBER_END_INDEX).trim();
                String regNumber = string.substring(REG_NUMBER_BEGIN_INDEX, REG_NUMBER_END_INDEX).trim();
                String passages = string.substring(PASSAGES_BEGIN_INDEX, PASSAGES_END_INDEX).trim();

                if (!passages.isEmpty() && KanalParserUtil.parseInt(passages)) {
                    entity.setPassages(Integer.parseInt(passages));
                    if (regNumber.matches(KanalParserUtil.REG_NUMBER_PATTERN)) {
                        entity.setRegNumber(regNumber);
                    } else if (!chipNumber.isEmpty()) {
                        entity.setChipNumber(chipNumber);
                    } else {
                        throw new KanalIllegalFormatException("%s: %s. Registration number or chip number should be specified.", Kanal5Validator.ERROR_KANAL_MSG, lineCounter);
                    }

                    if (entity.getChipNumber() == null && !chipNumber.isEmpty()) {
                        entity.setChipNumber(chipNumber);
                    }

                    String refComment = string.substring(REF_COMMENT_BEGIN_INDEX, REF_COMMENT_END_INDEX).trim();
                    if (!refComment.isEmpty()) {
                        entity.setRefComment(refComment);
                    } else {
                        throw new KanalIllegalFormatException("%s: %s. Reference comment isn't specified.", Kanal5Validator.ERROR_KANAL_MSG, lineCounter);
                    }
                    entity.setProject(string.substring(PROJECT_BEGIN_INDEX, PROJECT_END_INDEX).trim());
                    String course = string.substring(COURSE_BEGIN_INDEX, COURSE_END_INDEX).trim();
                    if (KanalParserUtil.parseDouble(course)) {
                        entity.setRate(KanalParserUtil.getOresFromStringKrones(course));
                    } else if (!course.isEmpty()) {
                        throw new KanalIllegalFormatException("%s: %s. Course specified %s is not numeric value.", Kanal5Validator.ERROR_KANAL_MSG, lineCounter, course);
                    }

                    String payment = string.substring(PAYMENT_BEGIN_INDEX).trim();
                    if (KanalParserUtil.parseDouble(payment)) {
                        entity.setSum(KanalParserUtil.getOresFromStringKrones(payment));
                    } else {
                        throw new KanalIllegalFormatException("%s: %s. Payment specified %s is not numeric value.", Kanal5Validator.ERROR_KANAL_MSG, lineCounter, payment);
                    }
                    entities.add(entity);
                }
            }
        }
        return createSummaryEntities(entities);
    }

    private List<Summary> createSummaryEntities(List<EntitySummary> entities) {
        List<Summary> summaries = new ArrayList<>();
        for (EntitySummary entity : entities) {
            findSummaryByChipNumber(summaries, entity);
        }
        return summaries;
    }

    /**
     * @param summaries          - list of created summaries prepared for invoice.
     * @param entityForSearching - entity with sum and passages.
     * @return edited list of summaries.
     */
    private List<Summary> findSummaryByChipNumber(List<Summary> summaries, EntitySummary entityForSearching) {
        if (!summaries.isEmpty()) {
            for (Summary summary : summaries) {
                if (summary.getChipNumber() != null && summary.getChipNumber().equals(entityForSearching.getChipNumber())) {
                    if (summary.getRefComment() == null && entityForSearching.getRefComment() != null) {
                        summary.setRefComment(entityForSearching.getRefComment());
                    }
                    summary.setTotalSum(summary.getTotalSum() + entityForSearching.getSum());
                    summary.setTotalPassages(summary.getTotalPassages() + entityForSearching.getPassages());
                    return summaries;
                }
            }
        }
        Summary summary = new Summary();
        summary.setRate(entityForSearching.getRate());
        summary.setRegNumber(entityForSearching.getRegNumber());
        summary.setChipNumber(entityForSearching.getChipNumber());
        summary.setRefComment(entityForSearching.getRefComment());
        summary.setTotalPassages(entityForSearching.getPassages());
        summary.setTotalSum(entityForSearching.getSum());
        summaries.add(summary);
        return summaries;
    }
}
