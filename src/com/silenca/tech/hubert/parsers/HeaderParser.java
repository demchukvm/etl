package com.silenca.tech.hubert.parsers;

import com.silenca.tech.hubert.exps.KanalIllegalFormatException;

/**
 * @author Helena Bragina on 4/22/14 1:03 PM.
 */
public class HeaderParser {

    private static final String EPLKNO_PATTERN = "EPLKNO[0-9]{3,4}[ ]{4}100";
    private static final String STRING_EPLKNO_PATTERN = "EPLKNO[A-Z]{3,4}[ ]{4}100";
    public static final int BEGIN_CODE_INDEX = 6;
    public static final int END_CODE_INDEX = 10;

    public static String parseAreaCode(String line, int lineNumber) throws KanalIllegalFormatException {
        if (line.matches(EPLKNO_PATTERN) || line.matches(STRING_EPLKNO_PATTERN)) {
            return line.substring(BEGIN_CODE_INDEX, END_CODE_INDEX).trim();
        }
        throw new KanalIllegalFormatException("Header, line %s: Illegal format of document header.\n  %s instead of expected format EPLKNO[0-9]{4}[ ]{4}100.",  lineNumber, line);
    }

}
