package com.silenca.tech.hubert.parsers;

import com.silenca.tech.hubert.entities.kanals.Kanal;
import com.silenca.tech.hubert.exps.KanalIllegalFormatException;

import java.util.List;

/**
 * @author Helena Bragina on 5/10/14 2:22 PM.
 *         Interface for parsers.
 */
public interface Parsable {

    /**
     * @param strings              - contains kanal strings for parsing.
     * @param lineNumberInDocument - line of first kanal string in whole document, uses for creating reports
     *                             about invoice mistakes.
     * @param kanalType            - contains page definition of invoice, kanal data often depends on this type.
     * @return object implemented interface Kanal.
     * @throws KanalIllegalFormatException
     */
    Kanal parse(List<String> strings, int lineNumberInDocument, String kanalType) throws KanalIllegalFormatException;
}
