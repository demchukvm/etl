package com.silenca.tech.hubert.parsers;

import com.silenca.tech.hubert.entities.kanals.Kanal2;
import com.silenca.tech.hubert.exps.KanalIllegalFormatException;
import com.silenca.tech.hubert.utils.KanalParserUtil;

import java.util.List;

/**
 * @author Helena Bragina on 4/16/14 4:12 PM.
 *         2.2, table1 in specification
 */
public class Kanal2Parser implements Parsable {

    public static final String KANAL_ERROR = "Kanal 2, line";

    private static final byte NAME_INDEX = 0;
    private static final byte ADDRESS_1_INDEX = 1;
    private static final byte ADDRESS_2_INDEX = 2;
    private static final byte POST_NUMBER_INDEX = 3;
    private static final byte COUNTRY_INDEX = 4;
    private static final byte ORGANIZATION_INDEX = 5;
    private static final byte IDENTIFICATION_INDEX = 6;

    private static final byte MIN_KANAL_STRINGS = 5;

    public Kanal2 parse(List<String> strings, int lineCounter, String invoiceDefinition) throws KanalIllegalFormatException {
        if (strings.size() >= MIN_KANAL_STRINGS) {
            for (String string : strings) {
                if (string == null || string.isEmpty()) {
                    throw new KanalIllegalFormatException("%s: %s. Illegal kanal 2 format! String in file is empty!", KANAL_ERROR, lineCounter);
                }
            }
            return getSection(strings, lineCounter);
        }
        throw new KanalIllegalFormatException("%s: %s. Illegal kanal 2 format! Size of strings %s < 5.", KANAL_ERROR, lineCounter, strings.size());
    }

    private Kanal2 getSection(List<String> strings, int lineCounter) throws KanalIllegalFormatException {
        // EPL50036 has 5 attributes, all another entities have 7 attributes in this kanal
        Kanal2 kanal = new Kanal2();
        String name = KanalParserUtil.removeNewLineChar(strings.get(NAME_INDEX), lineCounter).trim();
        if (!name.isEmpty()) {
            kanal.setName(name);
        } else {
            throw new KanalIllegalFormatException("%s: %s. Customer name isn't specified!", KANAL_ERROR, lineCounter);
        }
        lineCounter++;
        String address1 = KanalParserUtil.removeNewLineChar(strings.get(ADDRESS_1_INDEX), lineCounter).trim();
        if (!address1.isEmpty()) {
            kanal.setAddress1(address1);
        } else {
            throw new KanalIllegalFormatException("%s: %s. Customer address isn't specified!", KANAL_ERROR, lineCounter);
        }
        lineCounter++;
        String address2 = KanalParserUtil.removeNewLineChar(strings.get(ADDRESS_2_INDEX), lineCounter).trim();
        if (!address2.isEmpty()) {
            kanal.setAddress1(address2);
        }
        lineCounter++;
        String postNumber = KanalParserUtil.removeNewLineChar(strings.get(POST_NUMBER_INDEX), lineCounter).trim();
        if (!postNumber.isEmpty()) {
            kanal.setZipCode(postNumber);
        } else {
            throw new KanalIllegalFormatException("%s: %s. Customer post code isn't specified!", KANAL_ERROR, lineCounter);
        }
        lineCounter++;
        String country = KanalParserUtil.removeNewLineChar(strings.get(COUNTRY_INDEX), lineCounter).trim();
        if (!country.isEmpty()) {
            kanal.setCountry(country);
        }
        lineCounter++;
        if (strings.size() == Kanal2.ARGS_MAX_SIZE) {
            String organizationNumber = KanalParserUtil.removeNewLineChar(strings.get(ORGANIZATION_INDEX), lineCounter).trim();
            if (!organizationNumber.isEmpty()) {
                kanal.setOrganizationNumber(organizationNumber);
            } else {
                lineCounter++;
                String identificationNumber = KanalParserUtil.removeNewLineChar(strings.get(IDENTIFICATION_INDEX), lineCounter).trim();
                if (!identificationNumber.isEmpty()) {
                    kanal.setIdentificationNumber(identificationNumber);
                } else {
                    throw new KanalIllegalFormatException("%s: %s. Identification number isn't specified.", KANAL_ERROR, lineCounter);
                }
            }
        }
        return kanal;
    }


}
