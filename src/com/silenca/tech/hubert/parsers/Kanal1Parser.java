package com.silenca.tech.hubert.parsers;

import com.silenca.tech.hubert.entities.kanals.Kanal1;
import com.silenca.tech.hubert.exps.KanalIllegalFormatException;
import com.silenca.tech.hubert.utils.KanalParserUtil;

import java.util.List;

/**
 * @author Helena Bragina on 4/16/14 4:12 PM.
 */
public class Kanal1Parser implements Parsable {

    public static final String KANAL_ERROR = "Kanal 1, line";

    private static final byte FIRST_SECTION_LINE_INDEX = 0;
    private static final byte BILL_TYPE_INDEX = 1;
    private static final byte CLIENT_NAME_INDEX = 2;
    private static final byte ADDRESS_1_INDEX = 3;
    private static final byte ADDRESS_2_INDEX = 4;
    private static final byte POST_NUMBER_INDEX = 5;
    private static final byte COUNTRY_INDEX = 6;

    /**
     * @param strings     - strings with section 1 values.
     * @param lineCounter - useful for creating reports about errors.
     * @return entity with attributes parsed from file.
     * @throws com.silenca.tech.hubert.exps.KanalIllegalFormatException if format of section is wrong (missed type of bill, attribute values have wrong format).
     */
    public Kanal1 parse(List<String> strings, int lineCounter, String invoiceType) throws KanalIllegalFormatException {
        if (strings.size() >= 2) {
            for (String string : strings) {
                if (string == null || string.isEmpty()) {
                    throw new KanalIllegalFormatException("%s: %s. String in file is empty!", KANAL_ERROR, lineCounter);
                }
            }
            return getSection(strings, lineCounter);
        }
        throw new KanalIllegalFormatException("%s: %s. Unspecified type of the invoice!", KANAL_ERROR, ++lineCounter);
    }

    private Kanal1 getSection(List<String> strings, int lineNumber) throws KanalIllegalFormatException {
        Kanal1 kanal1 = new Kanal1();
        if (strings.get(FIRST_SECTION_LINE_INDEX).length() > 2) {
            // array values may contains number in the right side, first value will be type
            String[] values = strings.get(FIRST_SECTION_LINE_INDEX).split("\\s+");
            kanal1.setInvoiceType(values[0].substring(2));
        }
        String pageDefinition = strings.get(BILL_TYPE_INDEX);
        lineNumber++;
        if (KanalParserUtil.INVOICE_TYPE_PATTERN.matcher(pageDefinition).matches() && KanalParserUtil.checkInvoiceType(pageDefinition)) {
            kanal1.setPageDefinition(pageDefinition);
        } else {
            kanal1.setPageDefinition("");
            throw new KanalIllegalFormatException("%s: %s. Wrong format of the invoice type: %s.", KANAL_ERROR, lineNumber, pageDefinition);
        }
        return kanal1;
    }

    public void setAdditionalData(Kanal1 kanal, List<String> strings, int lineCounter) throws KanalIllegalFormatException {
        kanal.setClientsName(KanalParserUtil.removeNewLineChar(strings.get(CLIENT_NAME_INDEX), lineCounter));
        String address1 = KanalParserUtil.removeNewLineChar(strings.get(ADDRESS_1_INDEX), lineCounter);
        if (!address1.isEmpty()) {
            kanal.setAddress1(address1);
        }
        String address2 = KanalParserUtil.removeNewLineChar(strings.get(ADDRESS_2_INDEX), lineCounter);
        if (!address2.isEmpty()) {
            kanal.setAddress2(address2);
        }
        String postNumber = KanalParserUtil.removeNewLineChar(strings.get(POST_NUMBER_INDEX), lineCounter);
        if (!postNumber.isEmpty()) {
            kanal.setPostNumber(postNumber);
        }
        // country will be specified if isn't Norway
        String country = KanalParserUtil.removeNewLineChar(strings.get(COUNTRY_INDEX), lineCounter);
        if (!country.isEmpty()) {
            kanal.setCountry(country);
        }
    }

}
