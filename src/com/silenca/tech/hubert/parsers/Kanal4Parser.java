package com.silenca.tech.hubert.parsers;

import com.silenca.tech.hubert.entities.kanals.Kanal4;
import com.silenca.tech.hubert.exps.KanalIllegalFormatException;
import com.silenca.tech.hubert.utils.KanalParserUtil;

import java.util.List;

/**
 * @author Helena Bragina on 4/23/14 12:27 PM.
 */
public class Kanal4Parser implements Parsable {

    private static final String KANAL_ERROR = "Kanal 4, line";
    public static final int COMMENT_LINE_INDEX = 0;

    private int lineNumber;

    public Kanal4 parse(List<String> lines, int lineCounter, String invoiceDefinition) throws KanalIllegalFormatException {
        lineNumber = lineCounter;
        if (lines.isEmpty()) {
            throw new KanalIllegalFormatException("%s: %s. Line is empty.", KANAL_ERROR, lineNumber);
        }
        return getKanal(lines.get(COMMENT_LINE_INDEX));
    }

    private Kanal4 getKanal(String line) throws KanalIllegalFormatException {
        Kanal4 kanal4 = new Kanal4();
        String textValue = KanalParserUtil.removeNewLineChar(line, lineNumber);
        kanal4.setAdditionalText(textValue);
        return kanal4;
    }
}
