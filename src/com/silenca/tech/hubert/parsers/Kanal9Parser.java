package com.silenca.tech.hubert.parsers;

import com.silenca.tech.hubert.entities.kanals.Kanal9;
import com.silenca.tech.hubert.exps.KanalIllegalFormatException;

import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Helena Bragina on 4/15/14 7:45 PM.
 */
public class Kanal9Parser implements Parsable {

    /**
     * Index number of field value in string from file.
     */
    private static final byte KID_INDEX = 1;
    private static final byte CRONER_INDEX = 2;
    private static final byte ORE_INDEX = 3;
    private static final byte CHECK_SUM_INDEX = 4;
    private static final Pattern PATTERN = Pattern.compile("^90[ 0-9]{25} [0-9 ]{8} [0-9]{2} [0-9]$");
    private static final String KANAL = "Kanal 9, line";
    public static final int KANAL_LINE_INDEX = 0;

    public Kanal9 parse(List<String> string, int lineNumber, String invoiceDefinition) throws KanalIllegalFormatException {
        String line = string.get(KANAL_LINE_INDEX);
        if (PATTERN.matcher(line).matches()) {
            return getKanal(line);
        }
        throw new KanalIllegalFormatException("%s: %s. Wrong kanal format! Value = %s.\n  Required format: %s", KANAL, lineNumber, string, PATTERN.toString());
    }


    private Kanal9 getKanal(String string) throws KanalIllegalFormatException {
        Kanal9 section = new Kanal9();
        String[] values = string.split("\\s+");
        section.setKIDNumber(values[KID_INDEX]);
        String cronerString = values[CRONER_INDEX];
        section.setCroner(Integer.parseInt(cronerString));
        String oreValue = values[ORE_INDEX];
        section.setOre(Integer.parseInt(oreValue));

        String checkSumString = values[CHECK_SUM_INDEX];
        section.setCheckSum(Byte.parseByte(checkSumString));
        return section;
    }


}
