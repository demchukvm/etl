                     HUBERT INSTRUCTION

-------------------------------------------------------------------------------------

Requirements:

    
- prunsrv.exe

    
- Java 1.7



First Hubert run (if service not installed):

-----------------

   
    1. Change paths in start.bat.
    2. Install service: run installService.bat.
    3. Change path to prunsrv.exe in start.bat and run start.bat.



There are also utility program for setting service properties. To run this utulity start hubertManager.bat.

All commands for working with service are located in official documenation http://commons.apache.org/proper/commons-daemon/procrun.html.


To stop Hubert run stop.bat.

To run Hubert in debug mode start startInDebugMode.bat.

Hubert run (if service installed):

---------------------------------


    1. Run start.bat.


Main class of Hubert project is com/silenca/tech/hubert/main/HubertContext.java

Input data is located in directory /data.
Class com/silenca/tech/hubert/main/HubertRoutes.java contains chain of methods for performance.



=====================================================================================



Requirements for development Hubert:
 
	
     - Maven version 3.0.4;

	- JDK 1.7
 
	- Installed MSSQL server

====================================================================================


To run application (from sources):


-------------------

 
 - Download project;


  - Install maven on the local machine (check if maven installed, perform in console: mvn -version);

 
 - Open console/terminal, go to the project root directory (hubert), perform in console next operations:

    
      1. To clean, compile project: mvn clean compile;

    
      2. Run application: mvn camel:run.




If project is downloaded, you can start application with start.bat file. 


If input data parsed successfully, beans Invoice, Customer, Passage, Summary (created from this data) persist to DB
, preferences of DB are 
located in jpaAdapter in
 resources/META-INF/spring/camel-context.xml). And save tha data file into the directory specified in properties 
file
conf/camel.properties by key package.success.



Otherwise, the letter will be send to the e-mail specified in conf/camel.properties with keys remote.e-mail,
remote.password, you can change 
e-mail and password values for your ones to test, it's desirable to use Gmail
(in future it'll changed for all smtps). 
Data file will be saved 
into the directory specified in properties file
conf/camel.properties by key package.failed.




After parsing file Hubert waits for new one.


Money stored to DB in ore.


----------------------------------------

camel.properties - contains directories paths for reading document and saving, e-mails preferences (for sending reports).
----------------------------------------

resources/META-INF/persistence.xml contains properties for DB: connection URL, username, password.